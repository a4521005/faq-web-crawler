//specification/projector-faq-kn-00075
Given("Open the page successfully on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Open the page successfully on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});