//specification/projector-faq-kn-00075
Given("Open the page successfully on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Open the page successfully on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How do I upgrade firmware for better input lag experience of GS50?"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="The input lag of GS50 is 22.7ms (1080P@60Hz) under Game Mode, please visit the link below and follow the instruction to carry out firmware upgrades for your projectors."
    // const checkVideo="https://youtu.be/ON4oAwaGhCM"
    const checkVideo="ON4oAwaGhCM"
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("videofailURL: ",videofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});