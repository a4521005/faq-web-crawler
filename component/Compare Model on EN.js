//only one model
Then("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});


//more than one model
Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    // const checkModel=[
    //     "EH600", 
    //     "EW600", 
    //     "EW800ST", 
    //     "EX600", 
    //     "EX800ST", 
    //     "GS1", 
    //     "GS2", 
    //     "GS50", 
    //     "GV1", 
    //     "GV30", 
    //     "HT2650i", 
    //     "HT3550i", 
    //     "QS01", 
    //     "TH685i", 
    //     "TK700STi", 
    //     "TK810", 
    //     "TK850i", 
    //     "V7000i", 
    //     "V7050i", 
    //     "W1800i", 
    //     "W2700i", 
    //     "X1300i", 
    //     "X3000i", 
    //     "GV11"
    // ]
    const checkModel="EH600, EW600, EW800ST, EX600, EX800ST, GS1, GS2, GS50, GV1, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK810, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00001.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});


//have Show More button
Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW277HDR, EW2780U, EW3270U, EW3280U, EX2510, EX2710, EX2780Q, EX3203R, EX3501R, PD2700U, PD2720U, PD3220U, SW271, SW320, EW2880U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00002.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

