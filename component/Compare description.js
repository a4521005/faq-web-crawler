//Description+Video

Then("Compare description to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="The input lag of GS50 is 22.7ms (1080P@60Hz) under Game Mode, please visit the link below and follow the instruction to carry out firmware upgrades for your projectors."
    // const checkVideo="https://youtu.be/ON4oAwaGhCM"
    const checkVideo="ON4oAwaGhCM"
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("videofailURL: ",videofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});


//Description only

Then("Compare description to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="This results from the process of encoding and decoding audio and is unavoidable. Due to physics and distance, absolute zero latency is impossible, but there are two main ways to minimize lip sync issues:"
    const checkDescriptionTwo="Use an aptX or aptX-LL Bluetooth device to play audio. Note BenQ projectors only support the SBC (Sub-band Codec) audio coding format."
    const checkDescriptionThree="Instead of Bluetooth, consider using wired speakers or headphones."

    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div > div > p > span', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div > div > p > span', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

