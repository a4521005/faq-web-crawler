Feature:B2C BenQ FAQ Web Crawler
    1.the URL can open the pages successfully
    2.
    if EN site:
    Compare title to check pass or fail
    Compare description to check pass or fail
    Compare Model to check pass or fail
    else non-EN site:
    Compare Model to check pass or fail

# specification/trevolo-faq-kn-00011
    Scenario: specification/trevolo-faq-kn-00011
        Given Open the page successfully on specification-trevolo-faq-kn-00011
    Scenario: [Titles][EN-site]specification/trevolo-faq-kn-00011
        Then Compare title to check pass or fail on specification-trevolo-faq-kn-00011
    Scenario: [Description][EN-site]specification/trevolo-faq-kn-00011
        Then Compare description to check pass or fail on specification-trevolo-faq-kn-00011
    Scenario: [Models][EN-site]specification/trevolo-faq-kn-00011 on en site
        Then [EN site]Compare Model to check pass or fail on specification-trevolo-faq-kn-00011
    Scenario: [Models][non-EN site]specification/trevolo-faq-kn-00011 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on specification-trevolo-faq-kn-00011

# application/projector-faq-kn-00126
    Scenario: application/projector-faq-kn-00126
        Given Open the page successfully on application-projector-faq-kn-00126
    Scenario: [Titles][EN-site]application/projector-faq-kn-00126
        Then Compare title to check pass or fail on application-projector-faq-kn-00126
    Scenario: [Description][EN-site]application/projector-faq-kn-00126
        Then Compare description to check pass or fail on application-projector-faq-kn-00126


# explanation/projector-faq-kn-00028
    Scenario: explanation/projector-faq-kn-00028
        Given Open the page successfully on explanation-projector-faq-kn-00028
    Scenario: [Titles][EN-site]explanation/projector-faq-kn-00028
        Then Compare title to check pass or fail on explanation-projector-faq-kn-00028
    Scenario: [Description][EN-site]explanation/projector-faq-kn-00028
        Then Compare description to check pass or fail on explanation-projector-faq-kn-00028

# application/projector-faq-kn-00087
    Scenario: application/projector-faq-kn-00087
        Given Open the page successfully on application-projector-faq-kn-00087
    Scenario: [Titles][EN-site]application/projector-faq-kn-00087
        Then Compare title to check pass or fail on application-projector-faq-kn-00087
    Scenario: [Description][EN-site]application/projector-faq-kn-00087
        Then Compare description to check pass or fail on application-projector-faq-kn-00087

# application/projector-faq-kn-00104
    Scenario: application/projector-faq-kn-00104
        Given Open the page successfully on application-projector-faq-kn-00104
    Scenario: [Titles][EN-site]application/projector-faq-kn-00104
        Then Compare title to check pass or fail on application-projector-faq-kn-00104
    Scenario: [Description][EN-site]application/projector-faq-kn-00104
        Then Compare description to check pass or fail on application-projector-faq-kn-00104

# application/monitor-faq-kn-00032
    Scenario: application/monitor-faq-kn-00032
        Given Open the page successfully on application-monitor-faq-kn-00032
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00032
        Then Compare title to check pass or fail on application-monitor-faq-kn-00032
    Scenario: [Description][EN-site]application/monitor-faq-kn-00032
        Then Compare description to check pass or fail on application-monitor-faq-kn-00032
    Scenario: [Models][EN-site]application/monitor-faq-kn-00032 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00032 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032

# troubleshooting/monitor-faq-kn-00014
    Scenario: application/troubleshooting/monitor-faq-kn-00014
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00014
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00014
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00014
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00014 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00014 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014

# application/monitor-faq-kn-00021
    Scenario: application/monitor-faq-kn-00021
        Given Open the page successfully on application-monitor-faq-kn-00021
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00021
        Then Compare title to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Description][EN-site]application/monitor-faq-kn-00021
        Then Compare description to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Models][EN-site]application/monitor-faq-kn-00021 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00021 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021

# troubleshooting/monitor-faq-kn-00005
    Scenario: application/troubleshooting/monitor-faq-kn-00005
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00005
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00005
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00005
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00005
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00005
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00005 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00005 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005

# explanation/monitor-faq-kn-00010
    Scenario: explanation/monitor-faq-kn-00010
        Given Open the page successfully on explanation-monitor-faq-kn-00010
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00010
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00010
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00010
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00010
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00010 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00010
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00010 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00010

# explanation/monitor-faq-kn-00011
    Scenario: explanation/monitor-faq-kn-00011
        Given Open the page successfully on explanation-monitor-faq-kn-00011
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00011
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00011
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00011
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00011
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00011 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00011 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011

# specification/monitor-faq-kn-00018
    Scenario: specification/monitor-faq-kn-00018
        Given Open the page successfully on specification-monitor-faq-kn-00018
    Scenario: [Titles][EN-site]specification/monitor-faq-kn-00018
        Then Compare title to check pass or fail on specification-monitor-faq-kn-00018
    Scenario: [Description][EN-site]specification/monitor-faq-kn-00018
        Then Compare description to check pass or fail on specification-monitor-faq-kn-00018
    Scenario: [Models][EN-site]specification/monitor-faq-kn-00018 on en site
        Then [EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00018
    Scenario: [Models][non-EN site]specification/monitor-faq-kn-00018 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00018

# application/monitor-faq-kn-00045
    Scenario: application/monitor-faq-kn-00045
        Given Open the page successfully on application-monitor-faq-kn-00045
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00045
        Then Compare title to check pass or fail on application-monitor-faq-kn-00045
    Scenario: [Description][EN-site]application/monitor-faq-kn-00045
        Then Compare description to check pass or fail on application-monitor-faq-kn-00045
    Scenario: [Models][EN-site]application/monitor-faq-kn-00045 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00045 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045

# explanation/monitor-faq-kn-00009
    Scenario: explanation/monitor-faq-kn-00009
        Given Open the page successfully on explanation-monitor-faq-kn-00009
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00009
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00009
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00009
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00009
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00009 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00009 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009

# troubleshooting/monitor-faq-kn-00013
    Scenario: application/troubleshooting/monitor-faq-kn-00013
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00013
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00013
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00013
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00013
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00013
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00013 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00013 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013

# application/monitor-faq-kn-00046
    Scenario: application/monitor-faq-kn-00046
        Given Open the page successfully on application-monitor-faq-kn-00046
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00046
        Then Compare title to check pass or fail on application-monitor-faq-kn-00046
    Scenario: [Description][EN-site]application/monitor-faq-kn-00046
        Then Compare description to check pass or fail on application-monitor-faq-kn-00046
    Scenario: [Models][EN-site]application/monitor-faq-kn-00046 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00046 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046

# explanation/monitor-faq-kn-00007
    Scenario: explanation/monitor-faq-kn-00007
        Given Open the page successfully on explanation-monitor-faq-kn-00007
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00007
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00007
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00007
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00007
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00007 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00007 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007

# explanation/monitor-faq-kn-00008
    Scenario: explanation/monitor-faq-kn-00008
        Given Open the page successfully on explanation-monitor-faq-kn-00008
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00008
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00008
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00008
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00008
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00008 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00008 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008

# troubleshooting/monitor-faq-kn-00014
    Scenario: application/troubleshooting/monitor-faq-kn-00014
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00014
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00014
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00014
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00014 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00014 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014

# troubleshooting/monitor-faq-kn-00015
    Scenario: application/troubleshooting/monitor-faq-kn-00015
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00015
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00015
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00015
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00015
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00015
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00015 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00015 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015

# troubleshooting/monitor-faq-kn-00001
    Scenario: application/troubleshooting/monitor-faq-kn-00001
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00001
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00001
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00001
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00001
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00001
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00001 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00001 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001

# specification/monitor-faq-kn-00009
    Scenario: specification/monitor-faq-kn-00009
        Given Open the page successfully on specification-monitor-faq-kn-00009
    Scenario: [Titles][EN-site]specification/monitor-faq-kn-00009
        Then Compare title to check pass or fail on specification-monitor-faq-kn-00009
    Scenario: [Description][EN-site]specification/monitor-faq-kn-00009
        Then Compare description to check pass or fail on specification-monitor-faq-kn-00009
    Scenario: [Models][EN-site]specification/monitor-faq-kn-00009 on en site
        Then [EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00009
    Scenario: [Models][non-EN site]specification/monitor-faq-kn-00009 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00009

# application/monitor-faq-kn-00036
    Scenario: application/monitor-faq-kn-00036
        Given Open the page successfully on application-monitor-faq-kn-00036
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00036
        Then Compare title to check pass or fail on application-monitor-faq-kn-00036
    Scenario: [Description][EN-site]application/monitor-faq-kn-00036
        Then Compare description to check pass or fail on application-monitor-faq-kn-00036
    Scenario: [Models][EN-site]application/monitor-faq-kn-00036 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00036 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036

# application/monitor-faq-kn-00021
    Scenario: application/monitor-faq-kn-00021
        Given Open the page successfully on application-monitor-faq-kn-00021
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00021
        Then Compare title to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Description][EN-site]application/monitor-faq-kn-00021
        Then Compare description to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Models][EN-site]application/monitor-faq-kn-00021 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00021 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021

# application/monitor-faq-kn-00039
    Scenario: application/monitor-faq-kn-00039
        Given Open the page successfully on application-monitor-faq-kn-00039
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00039
        Then Compare title to check pass or fail on application-monitor-faq-kn-00039
    Scenario: [Description][EN-site]application/monitor-faq-kn-00039
        Then Compare description to check pass or fail on application-monitor-faq-kn-00039
    Scenario: [Models][EN-site]application/monitor-faq-kn-00039 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00039 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039

# troubleshooting/monitor-faq-kn-00006
    Scenario: application/troubleshooting/monitor-faq-kn-00006
        Given Open the page successfully on troubleshooting-monitor-faq-kn-00006
    Scenario: [Titles][EN-site]application/troubleshooting/monitor-faq-kn-00006
        Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00006
    Scenario: [Description][EN-site]application/troubleshooting/monitor-faq-kn-00006
        Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00006
    Scenario: [Models][EN-site]application/troubleshooting/monitor-faq-kn-00006 on en site
        Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006
    Scenario: [Models][non-EN site]application/troubleshooting/monitor-faq-kn-00006 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006

# explanation/monitor-faq-kn-00020
    Scenario: explanation/monitor-faq-kn-00020
        Given Open the page successfully on explanation-monitor-faq-kn-00020
    Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00020
        Then Compare title to check pass or fail on explanation-monitor-faq-kn-00020
    Scenario: [Description][EN-site]explanation/monitor-faq-kn-00020
        Then Compare description to check pass or fail on explanation-monitor-faq-kn-00020
    Scenario: [Models][EN-site]explanation/monitor-faq-kn-00020 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00020
    Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00020 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00020

# application/monitor-faq-kn-00050
    Scenario: application/monitor-faq-kn-00050
        Given Open the page successfully on application-monitor-faq-kn-00050
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00050
        Then Compare title to check pass or fail on application-monitor-faq-kn-00050
    Scenario: [Description][EN-site]application/monitor-faq-kn-00050
        Then Compare description to check pass or fail on application-monitor-faq-kn-00050
    Scenario: [Models][EN-site]application/monitor-faq-kn-00050 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00050 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050

# application/monitor-faq-kn-00008
    Scenario: application/monitor-faq-kn-00008
        Given Open the page successfully on application-monitor-faq-kn-00008
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00008
        Then Compare title to check pass or fail on application-monitor-faq-kn-00008
    Scenario: [Description][EN-site]application/monitor-faq-kn-00008
        Then Compare description to check pass or fail on application-monitor-faq-kn-00008
    Scenario: [Models][EN-site]application/monitor-faq-kn-00008 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00008 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008

# application/monitor-faq-kn-00051
    Scenario: application/monitor-faq-kn-00051
        Given Open the page successfully on application-monitor-faq-kn-00051
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00051
        Then Compare title to check pass or fail on application-monitor-faq-kn-00051
    Scenario: [Description][EN-site]application/monitor-faq-kn-00051
        Then Compare description to check pass or fail on application-monitor-faq-kn-00051
    Scenario: [Models][EN-site]application/monitor-faq-kn-00051 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00051 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051

# application/monitor-faq-kn-00009
    Scenario: application/monitor-faq-kn-00009
        Given Open the page successfully on application-monitor-faq-kn-00009
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00009
        Then Compare title to check pass or fail on application-monitor-faq-kn-00009
    Scenario: [Description][EN-site]application/monitor-faq-kn-00009
        Then Compare description to check pass or fail on application-monitor-faq-kn-00009
    Scenario: [Models][EN-site]application/monitor-faq-kn-00009 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00009 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009

# application/monitor-faq-kn-00007
    Scenario: application/monitor-faq-kn-00007
        Given Open the page successfully on application-monitor-faq-kn-00007
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00007
        Then Compare title to check pass or fail on application-monitor-faq-kn-00007
    Scenario: [Description][EN-site]application/monitor-faq-kn-00007
        Then Compare description to check pass or fail on application-monitor-faq-kn-00007
    Scenario: [Models][EN-site]application/monitor-faq-kn-00007 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00007 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007

# application/monitor-faq-kn-00006
    Scenario: application/monitor-faq-kn-00006
        Given Open the page successfully on application-monitor-faq-kn-00006
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00006
        Then Compare title to check pass or fail on application-monitor-faq-kn-00006
    Scenario: [Description][EN-site]application/monitor-faq-kn-00006
        Then Compare description to check pass or fail on application-monitor-faq-kn-00006
    Scenario: [Models][EN-site]application/monitor-faq-kn-00006 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00006 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006

# application/monitor-faq-kn-00029
    Scenario: application/monitor-faq-kn-00029
        Given Open the page successfully on application-monitor-faq-kn-00029
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00029
        Then Compare title to check pass or fail on application-monitor-faq-kn-00029
    Scenario: [Description][EN-site]application/monitor-faq-kn-00029
        Then Compare description to check pass or fail on application-monitor-faq-kn-00029
    Scenario: [Models][EN-site]application/monitor-faq-kn-00029 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00029 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029

# application/monitor-faq-kn-00005
    Scenario: application/monitor-faq-kn-00005
        Given Open the page successfully on application-monitor-faq-kn-00005
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00005
        Then Compare title to check pass or fail on application-monitor-faq-kn-00005
    Scenario: [Description][EN-site]application/monitor-faq-kn-00005
        Then Compare description to check pass or fail on application-monitor-faq-kn-00005
    Scenario: [Models][EN-site]application/monitor-faq-kn-00005 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00005 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005

# application/monitor-faq-kn-00013
    Scenario: application/monitor-faq-kn-00013
        Given Open the page successfully on application-monitor-faq-kn-00013
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00013
        Then Compare title to check pass or fail on application-monitor-faq-kn-00013
    Scenario: [Description][EN-site]application/monitor-faq-kn-00013
        Then Compare description to check pass or fail on application-monitor-faq-kn-00013

# application/monitor-faq-kn-00052
    Scenario: application/monitor-faq-kn-00052
        Given Open the page successfully on application-monitor-faq-kn-00052
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00052
        Then Compare title to check pass or fail on application-monitor-faq-kn-00052
    Scenario: [Description][EN-site]application/monitor-faq-kn-00052
        Then Compare description to check pass or fail on application-monitor-faq-kn-00052

# application/monitor-faq-kn-00053
    Scenario: application/monitor-faq-kn-00053
        Given Open the page successfully on application-monitor-faq-kn-00053
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00053
        Then Compare title to check pass or fail on application-monitor-faq-kn-00053
    Scenario: [Description][EN-site]application/monitor-faq-kn-00053
        Then Compare description to check pass or fail on application-monitor-faq-kn-00053


# application/monitor-faq-kn-00011
    Scenario: application/monitor-faq-kn-00011
        Given Open the page successfully on application-monitor-faq-kn-00011
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00011
        Then Compare title to check pass or fail on application-monitor-faq-kn-00011
    Scenario: [Description][EN-site]application/monitor-faq-kn-00011
        Then Compare description to check pass or fail on application-monitor-faq-kn-00011
    Scenario: [Models][EN-site]application/monitor-faq-kn-00011 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00011 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011

# application/projector-faq-kn-00076
    Scenario: application/projector-faq-kn-00076
        Given Open the page successfully on application-projector-faq-kn-00076
    Scenario: [Titles][EN-site]application/projector-faq-kn-00076
        Then Compare title to check pass or fail on application-projector-faq-kn-00076
    Scenario: [Description][EN-site]application/projector-faq-kn-00076
        Then Compare description to check pass or fail on application-projector-faq-kn-00076
    Scenario: [Models][EN-site]application/projector-faq-kn-00076 on en site
        Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076
    Scenario: [Models][non-EN site]application/projector-faq-kn-00076 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076

# application/projector-faq-kn-00070
    Scenario: application/projector-faq-kn-00070
        Given Open the page successfully on application-projector-faq-kn-00070
    Scenario: [Titles][EN-site]application/projector-faq-kn-00070
        Then Compare title to check pass or fail on application-projector-faq-kn-00070
    Scenario: [Description][EN-site]application/projector-faq-kn-00070
        Then Compare description to check pass or fail on application-projector-faq-kn-00070
    Scenario: [Models][EN-site]application/projector-faq-kn-00070 on en site
        Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070
    Scenario: [Models][non-EN site]application/projector-faq-kn-00070 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070

# application/monitor-faq-kn-00047
    Scenario: application/monitor-faq-kn-00047
        Given Open the page successfully on application-monitor-faq-kn-00047
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00047
        Then Compare title to check pass or fail on application-monitor-faq-kn-00047
    Scenario: [Description][EN-site]application/monitor-faq-kn-00047
        Then Compare description to check pass or fail on application-monitor-faq-kn-00047
    Scenario: [Models][EN-site]application/monitor-faq-kn-00047 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00047 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047

# application/monitor-faq-kn-00048
    Scenario: application/monitor-faq-kn-00048
        Given Open the page successfully on application-monitor-faq-kn-00048
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00048
        Then Compare title to check pass or fail on application-monitor-faq-kn-00048
    Scenario: [Description][EN-site]application/monitor-faq-kn-00048
        Then Compare description to check pass or fail on application-monitor-faq-kn-00048
    Scenario: [Models][EN-site]application/monitor-faq-kn-00048 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00048 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048

# application/monitor-faq-kn-00049
    Scenario: application/monitor-faq-kn-00049
        Given Open the page successfully on application-monitor-faq-kn-00049
    Scenario: [Titles][EN-site]application/monitor-faq-kn-00049
        Then Compare title to check pass or fail on application-monitor-faq-kn-00049
    Scenario: [Description][EN-site]application/monitor-faq-kn-00049
        Then Compare description to check pass or fail on application-monitor-faq-kn-00049
    Scenario: [Models][EN-site]application/monitor-faq-kn-00049 on en site
        Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049
    Scenario: [Models][non-EN site]application/monitor-faq-kn-00049 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049