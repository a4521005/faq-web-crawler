const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect, use } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
// const request = require("request-promise-native");
const cheerio = require("cheerio");

const b2cBenqComEnRegions = [
    "en-us",
    "en-hk",
    "en-ca",
    "en-ap",
    "en-au",
    "en-my",
    "en-in",
    "en-sg",
    "en-me"
]
const b2cBenqEuEnRegions = [
    "en-eu",
    "en-uk",
    "en-cee",
    "en-no",
    "en-fi",
    "en-dk",
    "en-ie"
]
const b2cBenqComNonEnRegions = [
    "zh-hk",
    "ru-ru",
    "fr-ca",
    "es-mx",
    "pt-br",
    "es-ar",
    "es-co",
    "es-pe",
    "es-la",
    "es-cl",
    "ar-me",
    "tr-tr",
    "id-id",
    "th-th",
    "ja-jp",
    "ko-kr",
    "vi-vn",
    "zh-tw"
]
const b2cBenqEuNonEnRegions = [
    "es-es",
    "it-it",
    "fr-fr",
    "nl-nl",
    "de-de",
    "pl-pl",
    "cs-cz",
    "sv-se",
    "ro-ro",    
    "uk-ua",    
    "lt-lt",
    "nl-be",
    "de-at",
    "de-ch"
]
const b2cBenqZhcnRegions = [
    "zh-cn"
]

const benqComWeb = "https://www.benq.com/"
const benqEuWeb = "https://www.benq.eu/"
const benqZhcnWeb = "https://www.benq.com.cn/"


const supportUrl = "/support/downloads-faq/faq/product/"
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1440,height:1000})
    await this.page.setDefaultTimeout(100000000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(100000000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//specification/trevolo-faq-kn-00011
Given("Open the page successfully on specification-trevolo-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/trevolo-faq-kn-00011
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/trevolo-faq-kn-00011
    const url = "specification/trevolo-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/trevolo-faq-kn-00011.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification/trevolo-faq-kn-00011 failURL: ",failURL)
    console.log("Open the page successfully on specification/trevolo-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-trevolo-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is the recommended ambient temperature for using the speaker?"
    const url = "specification/trevolo-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification/trevolo-faq-kn-00011 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification/trevolo-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-trevolo-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="The ideal surrounding temperature for using the speaker is roughly between 0~35°Celsius. If the speaker have been used for a prolonged period under extreme temperature, it may affect the overall performance and shorten the battery life."
    const checkDescriptionTwo="Therefore, it is highly recommended to avoid placing the speaker in any of the following conditions:"
    const checkDescriptionThree="On a heating pad."
    const checkDescriptionFour="Inside a car."
    const checkDescriptionFive="On top of an electric blanket."
    const checkDescriptionSix="Under direct sunlight."
    const url = "specification/trevolo-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0 || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0 || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-trevolo-faq-kn-00011 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-trevolo-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-trevolo-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="treVolo U"
    const url = "specification/trevolo-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification/trevolo-faq-kn-00011 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification/trevolo-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-trevolo-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="treVolo U"
    const url = "specification/trevolo-faq-kn-00011.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[non-EN site]Compare Model to check pass or fail on specification/trevolo-faq-kn-00011 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification/trevolo-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});



//application/projector-faq-kn-00126
Given("Open the page successfully on application-projector-faq-kn-00126",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    const url = "application/projector-faq-kn-00126.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00126 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00126 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00126",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How do I clean my projector lens?"
    const url = "application/projector-faq-kn-00126.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00126 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00126 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00126",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const checkDescriptionOne="It is normal to have some dust on the lens surface."
    const checkDescriptionTwo="Please refer to the below video for a guide to cleaning your projector's lens."
    // const checkVideo="https://youtu.be/ON4oAwaGhCM"
    const checkVideo="xt6Eyi5K0d4"


    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    const url = "application/projector-faq-kn-00126"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00126 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00126 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL} `)
    }
});

// explanation/projector-faq-kn-00028
Given("Open the page successfully on explanation-projector-faq-kn-00028",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/projector-faq-kn-00028.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00028.html
    const url = "explanation/projector-faq-kn-00028.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00028.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-projector-faq-kn-00028 failURL: ",failURL)
    console.log("Open the page successfully on explanation-projector-faq-kn-00028 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-projector-faq-kn-00028",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What can I do if my projection screen shrinks when Duplicate Mode is applied in Windows?"
    const url = "explanation/projector-faq-kn-00028.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-projector-faq-kn-00028 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-projector-faq-kn-00028 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-projector-faq-kn-00028",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescriptionOne="The displayed image ratio shrinks because the projector adjusts the displayed image ratio based on a received resolution which does not match the output resolutions the projector is designed for."
    const checkDescriptionTwo="Please follow the steps below if you would like to have the displayed image remain intact:"
    const checkDescriptionThree="Step 1"    
    const checkDescriptionFour="Go to Windows Settings and select Display" 
    const checkDescriptionFive="Step 2"    
    const checkDescriptionSix="Select Advanced Display Settings" 
    const checkDescriptionSeven="Step 3"    
    const checkDescriptionEight="Select Display 2: BenQ PJ, then click Display Adapter Properties for Display 2"     
    const checkDescriptionNine="Step 4"    
    const checkDescriptionTen="Select List All Modes, then choose an output resolution that your projector is designed for (as listed)"         
    const checkPhotoOne="Duplicate Mode_1."
    const checkPhotoTwo="Duplicate Mode_2"
    const checkPhotoThree="Duplicate Mode_3"
    const checkPhotoFour="Duplicate Mode_4"
    const checkPhotoFive="Duplicate Mode_5"
    const url = "explanation/projector-faq-kn-00028.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0){
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }

            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0){
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }

            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandphotofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00028 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00028 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }    
});

//application/projector-faq-kn-00087
Given("Open the page successfully on application-projector-faq-kn-00087",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00087.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00087.html
    const url = "application/projector-faq-kn-00087.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00087.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00087 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00087 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00087",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Is there a projector that supports Blu-ray 3D movie viewing with Passive Polarized Glasses, like on my TV?"
    const url = "application/projector-faq-kn-00087.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00087 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00087 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00087",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="No. Projectors only work with active 3D display standards. Passive 3D is now considered obsolete and thus isn't supported by projectors."

    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00087 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00087 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//application/projector-faq-kn-00104
Given("Open the page successfully on application-projector-faq-kn-00104",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00104.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00104.html
    const url = "application/projector-faq-kn-00104.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00104.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00104 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00104 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00104",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Is there a projector that supports Blu-ray 3D movie viewing with Passive Polarized Glasses, like on my TV?"
    const url = "application/projector-faq-kn-00104.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00104 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00104 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00104",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="No. Projectors only work with active 3D display standards. Passive 3D is now considered obsolete and thus isn't supported by projectors."

    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00104 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00104 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//application/monitor-faq-kn-00032
Given("Open the page successfully on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00032.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00032.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is IPS glow and how can I make it less visible?"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Due to the technology used on IPS panels, some monitors have inconsistent brightness when observed from different angles and distances. This appears like a glow that’s somewhat visible when the screen is either all black or showing dark content. As IPS glow is technology-related, it cannot be completely prevented or eliminated. Thus, it has been widely accepted as a unique feature of IPS panels. This phenomenon is exclusive to IPS monitors and is far more common than TN/VA light bleed, although not as severe when it does happen."
    const checkDescriptionTwo="There are some setup tips that you can try to make the IPS glow less visible:"
    const checkDescriptionThree="Increase the ambient lighting in the room where the monitor is being used."
    const checkDescriptionFour="Lower the brightness level of the monitor via the OSD menu."
    const checkDescriptionFive="Adjust the height and tilt of the monitor until you find the right angle where the glow is least visible."
    const checkDescriptionSix="Place the monitor a little bit further from your seating position."

    const url = "application/monitor-faq-kn-00032.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2381T, BL2411PT, BL2420PT, BL2423PT, BL2480, BL2480L, BL2480T, BL2480TL, BL2485TC, BL2581T, BL2706HT, BL2711U, BL2780, BL2780T, BL2785TC, EW2480, EW2770QZ, EW2780, EW2780Q, EW2780U, EW2880U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2710U, EX2780Q, EX3210U, EX3415R, GW2283, GW2381, GW2406Z, GW2475H, GW2480, GW2480EL, GW2480L, GW2480T, GW2480TL, GW2485TC, GW2765HE, GW2765HT, GW2780, GW2780T, GW2785TC, PD2500Q, PD2700Q, PD2700QT, PD2700U, PD2705Q, PD2705U, PD2710QC, PD2720U, PD2725U, PD3200U, PD3200UE, PD3205U, PD3220U, PD3420Q, PG2401PT, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C, EX240, PD2506Q"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//troubleshooting/monitor-faq-kn-00014
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00014.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00014.html
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00014.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00014 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why doesn’t my USB-C cable work properly?"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="1. Please make sure your laptop's USB-C port supports DisplayPort Alt mode."
    const checkDescriptionTwo="2. Please make sure the purchased cable is certified by USB-IF and has feature include power delivery and video/audio/data transfer functionality."
    const checkDescriptionThree="3. It is recommended to use the certified USB-C cable which is attached in the BenQ product box"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2780Q, EX3203R, EX3501R, GW2485TC, GW2785TC, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271, SW271C, SW321C, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2780Q, EX3203R, EX3501R, GW2485TC, GW2785TC, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271, SW271C, SW321C, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00021
Given("Open the page successfully on application-monitor-faq-kn-00021",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00021.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00021.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00021 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00021 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00021",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Can I use USB-C to charge my Windows laptop or MacBook Pro?"
    const url = "application/monitor-faq-kn-00021.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00021 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00021 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00021",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="Yes, you can. While using USB-C to transmit audio/video, you can also deliver up to 60~90W of power to your laptop. However, keep in mind several different hardware configurations of USB-C exist and some do not support all forms of data and power delivery. Check with your laptop manufacturer to make sure your USB-C port offers DisplayPort Alt Mode compatibility and power delivery support."

    const url = "application/monitor-faq-kn-00021.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00021 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00021 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW3280U, EW3880R, EX480UZ, GW2485TC, GW2785TC, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271C, SW321C, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "application/monitor-faq-kn-00021.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00021 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00021 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW3280U, EW3880R, EX480UZ, GW2485TC, GW2785TC, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271C, SW321C, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "application/monitor-faq-kn-00021.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00021 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// troubleshooting/monitor-faq-kn-00005
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "troubleshooting/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00005.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How to configure the built-in noise-cancellation microphone of the monitor?"
    const url = "troubleshooting/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescriptionOne="1. Connect the monitor to your device with a certified USB-C cable."
    const checkDescriptionTwo="2. Turn on the built-in microphone with the hotkey at the bottom of the monitor."
    const checkDescriptionThree="3. Make sure you select the microphone of the monitor in your operating system settings."
    const checkPhoto="LCD_GW2790QT"

    const url = "application/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
             if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                if(photo.indexOf(checkPhoto)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(photo.indexOf(checkPhoto)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                if(photo.indexOf(checkPhoto)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(photo.indexOf(checkPhoto)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandphotofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GW2485TC, GW2785TC, BL2485TC, BL2785TC, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "troubleshooting/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GW2485TC, GW2785TC, BL2485TC, BL2785TC, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "troubleshooting/monitor-faq-kn-00005.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// explanation/monitor-faq-kn-00010
Given("Open the page successfully on explanation-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00010.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00010.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is BenQ Burn-In Cleaner?"
    const url = "explanation/monitor-faq-kn-00010.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescriptionOne="BenQ Burn-In Cleaner is designed to eliminate image sticking."
    const checkDescriptionTwo="Screen burn-in or image sticking may occur if a still image is displayed on screen for an extended period of time. Enable BenQ Burn-In Cleaner to help reduce burn-in that may be caused by retained lines, shades, or patterns that have remained on the screen for prolonged periods."
    const checkDescriptionThree="To do this, the monitor shows three different patterns and switches between these patterns rapidly. The recommended execution period is 30 minutes."
    const checkDescriptionFour="Please check the image stick or burn-in again after 30 mins. If still visible, please keep running Burn-In Cleaner until the issue is resolved."
    const checkDescriptionFive="If the image sticking issue disappears, press any of the control keys to stop the process."
    const checkDescriptionSix="How to enable this function"
    const checkDescriptionSeven="1. Select Menu from the hotkey menu "
    const checkDescriptionEight="2. Select System"
    const checkDescriptionNine="3. Go to the subpage Burn-in Cleaner and select YES to enable it."
    const checkDescriptionTen="4. Keep it running for around 30 minutes."
    const checkDescriptionEleven="5. Press any of the control keys to disable it.  "
    const checkPhoto="LCD-Burn-in cleaner"

    const url = "explanation/monitor-faq-kn-00010.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
             if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0|| description.indexOf(checkDescriptionFour)<0|| description.indexOf(checkDescriptionFive)<0|| description.indexOf(checkDescriptionSix)<0|| description.indexOf(checkDescriptionSeven)<0|| description.indexOf(checkDescriptionEight)<0|| description.indexOf(checkDescriptionNine)<0|| description.indexOf(checkDescriptionTen)<0|| description.indexOf(checkDescriptionEleven)<0){
                if(photo.indexOf(checkPhoto)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(photo.indexOf(checkPhoto)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0|| description.indexOf(checkDescriptionFour)<0|| description.indexOf(checkDescriptionFive)<0|| description.indexOf(checkDescriptionSix)<0|| description.indexOf(checkDescriptionSeven)<0|| description.indexOf(checkDescriptionEight)<0|| description.indexOf(checkDescriptionNine)<0|| description.indexOf(checkDescriptionTen)<0|| description.indexOf(checkDescriptionEleven)<0){
                if(photo.indexOf(checkPhoto)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(photo.indexOf(checkPhoto)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandphotofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705U, PD2705UA, PD2725U, PD3205U, PD3205UA, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00010.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00010 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705U, PD2705UA, PD2725U, PD3205U, PD3205UA, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00010.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00010 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// explanation/monitor-faq-kn-00011
Given("Open the page successfully on explanation-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00011.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is uniformity technology?"
    const url = "explanation/monitor-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="The reason why nonuniformity occurs on a screen is the unevenness of certain physical elements in the panel, which cause brightness or color inconsistencies at different parts of the panel. These include unevenness of the backlight or the unevenness of the light guide module. When this happens, manufacturers can compensate for the unevenness to improve uniformity of the screen. If the screen brightness or color saturation is not uniform, it can be calibrated to adjust and correct screen uniformity."
    const checkDescriptionTwo="BenQ Uniformity Technology offers screen-wide precision The reason why nonuniformity occurs on a screen is the unevenness of certain physical elements in the panel, which cause brightness or color inconsistencies at different parts of the panel. These include unevenness of the backlight or the unevenness of the light guide module. When this happens, manufacturers can compensate for the unevenness to improve uniformity of the screen. If the screen brightness or color saturation is not uniform, it can be calibrated to adjust and correct screen uniformity"
    const checkDescriptionThree="BenQ Uniformity Technology offers screen-wide precision from corner to corner with fine-tuned color and brightness on hundreds of sub-regions across the screen. This uniformity is created using high precision technology to make sure you see every scene with persistent authenticity and impeccable image fidelity."
    const checkDescriptionFour="For more detail, learn more about it here: "
    const checkDescriptionFive="Learn more"
    const checkDescriptionSix="https://www.benq.com/en-us/knowledge-center/knowledge/screen-uniformity.html"


    const url = "explanation/monitor-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree) || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0 || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0 || description.indexOf(checkDescriptionSix)<0)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)

    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705U, PD2705UA, PD2725U, PD3205U, PD3205UA, PD3420Q, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00011 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705U, PD2705UA, PD2725U, PD3205U, PD3205UA, PD3420Q, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00011.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00011 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//specification/monitor-faq-kn-00018
Given("Open the page successfully on specification-monitor-faq-kn-00018",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/monitor-faq-kn-00018
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/monitor-faq-kn-00018
    const url = "specification/monitor-faq-kn-00018.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/monitor-faq-kn-00018.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification/monitor-faq-kn-00018 failURL: ",failURL)
    console.log("Open the page successfully on specification/monitor-faq-kn-00018 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-monitor-faq-kn-00018",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why does the monitor not enter standby mode under PBP/PIP when I shut down my input devices such as laptops and desktops?"
    const url = "specification/monitor-faq-kn-00018.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification/monitor-faq-kn-00018 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification/monitor-faq-kn-00018 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-monitor-faq-kn-00018",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="This device behavior is not in error, and by design. If you would like to enter standby mode, please switch back to a signal input that is able to enter standby mode when your device is in sleep."
    
    const url = "specification/monitor-faq-kn-00018.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00018 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00018 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00018",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2700U, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "specification/monitor-faq-kn-00018.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00018 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00018 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00018",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2700U, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "specification/monitor-faq-kn-00018.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[non-EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00018 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00018 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00045
Given("Open the page successfully on application-monitor-faq-kn-00045",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00045.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00045.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00045 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00045 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00045",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I purchase an extension micro USB cable for HotKey Puck/HotKey Puck G2?"
    const url = "application/monitor-faq-kn-00045.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00045 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00045 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00045",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="The micro USB port of the monitor doesn’t follow standard USB protocols and cable length is related to signal attenuation. As such, BenQ does not guarantee 3rd party extension micro USB cables can work without compromising functionality and quality."

    const url = "application/monitor-faq-kn-00045.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00045 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00045 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C, PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00045.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00045 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00045 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C, PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00045.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00045 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00009
Given("Open the page successfully on explanation-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00009.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What are the benefits of HotKey Puck G2?"
    const url = "explanation/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00009.html failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00009.html passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="HotKey Puck G2 has three function keys and a rotating dial key, allowing users to designate preferred color modes and features such as shortcuts to widely used settings. The dial makes it extra easy to adjust brightness, contrast, and volume."

    const url = "explanation/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on explanation/monitor-faq-kn-00009.html failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation/monitor-faq-kn-00009.html passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW321C,  PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW321C,  PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00009.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
})

//troubleshooting/monitor-faq-kn-00013
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00013.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00013.html
    const url = "troubleshooting/monitor-faq-kn-00013.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00013.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00013 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why Apple verify that “DisplayPilot.app” is malicious software and cannot open it?"
    const url = "troubleshooting/monitor-faq-kn-00013.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="Apple enforced app notarization for MacOS Catalina in Feb, 2020."
    const checkDescriptionTwo="Please download the V1.0.6.7 or later version of DisplayPilot from BenQ official website to solve this issue."

    const url = "troubleshooting/monitor-faq-kn-00013.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00013.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00013.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00046
Given("Open the page successfully on application-monitor-faq-kn-00046",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00046.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00046.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00046 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00046 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00046",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I display pictures in 10-bit color?"
    const url = "application/monitor-faq-kn-00046.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00046 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00046 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00046",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please follow below steps to display pictures in 10-bit color:"
    const checkDescriptionTwo="(1) Ensure RAW/TIF files support 10-bit (30-bit)."
    const checkDescriptionThree="(2) Install software that supports 10-bit (30-bit), such as Photoshop or Lightroom."
    const checkDescriptionFour="(3) Install a graphics card that supports 10 bits (30-bit), such as Nvidia Quadro or AMD FireGL/Radeon Pro series."
    const checkDescriptionFive="(4) Connect your PC/laptop to the monitor with DisplayPort/HDMI/USB TypeC/Thouderbolt3 to display the pictures (Please go to BenQ website and download the resolution file of your monitor to select the available input)"

    const url = "application/monitor-faq-kn-00046.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00046 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00046 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2700Q, PD2700U, PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C,  PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00046.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00046 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00046 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2700Q, PD2700U, PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C,  PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00046.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00046 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00007
Given("Open the page successfully on explanation-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00007.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00007.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00007 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I reproduce the color profiles of a MacBook Pro on my BenQ monitor?"
    const url = "explanation/monitor-faq-kn-00007.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00007.html failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00007.html passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="Since the color profiles of a MacBook Pro are recognized as a standard among Mac users, BenQ has developed a new M-Book mode to simulate the color specs of MacBook Pro on our monitors. Switch to M-Book Mode to make your BenQ monitor emulate a MacBook Pro."

    const url = "explanation/monitor-faq-kn-00007.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on explanation/monitor-faq-kn-00007.html failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation/monitor-faq-kn-00007.html passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00007.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00007 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00007.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00007 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
})

// explanation/monitor-faq-kn-00008
Given("Open the page successfully on explanation-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00008.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00008.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How does Display Pilot software facilitate better use of your BenQ DesignVue monitor?"
    const url = "monitor-faq-kn-00008.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="Display Pilot is software which integrates quick and easy access for you to best utilize your monitor."
    const checkDescriptionTwo="Desktop Partition: splits your workspace easily in various scenarios without cumbersome dragging and resizing."
    const checkDescriptionThree="Print Assist: helps you get a good idea of what your work will look like on a printed hard copy and preview results compared to the screen."
    const checkDescriptionFour="Keyboard Hotkey: creates shortcuts to gain access to different color modes, video inputs, and DualView mode without taking your hands off the keyboard."
    const checkDescriptionFive="Application Mode: helps assign color modes and switch to different color modes for your chosen apps and various projects."


    const url = "explanation/monitor-faq-kn-00008.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0)){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0)){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)

    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700QT, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00008.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00008 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700QT, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00008.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00008 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});


//troubleshooting/monitor-faq-kn-00015
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00015.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00015.html
    const url = "troubleshooting/monitor-faq-kn-00015.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00015.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00015 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How come my monitor has no image when connecting to a Mac through USB-C?"
    const url = "troubleshooting/monitor-faq-kn-00015.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="Please make sure to connect your Mac and monitor with a BenQ USB-C cable or a USB-C cable which is certified by the USB-IF and features power delivery as well as full video, audio, and data transmission. Some USB-C cables for Mac are USB-C charge cables and do not support video. Also, please make sure the correct resolution and refresh rate are selected on your Mac and the monitor."

    const url = "troubleshooting/monitor-faq-kn-00015.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2780Q, EX3203R, EX3501R, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00015.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2780Q, EX3203R, EX3501R, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00015.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//troubleshooting/monitor-faq-kn-00001
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00001.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00001.html
    const url = "troubleshooting/monitor-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00001.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00001 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00001 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why is there no color mode option or why are there only specific color mode options for user selection in Display Pilot software?"
    const url = "troubleshooting/monitor-faq-kn-00001.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00001 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="Please enter the monitor OSD menu > System > Advanced > DDC/CI > ON to allow monitor settings to be set through the software on PC."
    const checkDescriptionTwo="Note: DDC/CI, short for Display Data Channel/Command Interface, was developed by the Video Electronics Standards Association (VESA). DDC/CI capability allows monitor controls to be sent via software for remote diagnostics."

    const url = "troubleshooting/monitor-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00001 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q,  PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00001.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q,  PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00001.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//specification/monitor-faq-kn-00009
Given("Open the page successfully on specification-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/monitor-faq-kn-00009
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/monitor-faq-kn-00009
    const url = "specification/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/monitor-faq-kn-00009.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Open the page successfully on specification/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What models do support ICC Sync in Display Pilot software?"
    const url = "specification/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="ICC sync of Display Pilot only supports specific models."
    
    const url = "specification/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q,  PD2706U, PD2706UA"
    const url = "specification/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q,  PD2706U, PD2706UA"
    const url = "specification/monitor-faq-kn-00009.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[non-EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification/monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00036
Given("Open the page successfully on application-monitor-faq-kn-00036",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00036.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00036.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00036 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00036 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00036",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why does the USB power notice always appear on my monitor when plugging a USB device? How do I get rid of the USB power notice?"
    const url = "application/monitor-faq-kn-00036.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00036 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00036 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00036",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Follow the steps below to enable USB power delivery in standby mode to get rid of the notice:"
    const checkDescriptionTwo="OSD menu>System>USB-C Awake>ON "
    const checkDescriptionThree="(PD3420Q/PD2705Q/PD2705U/PD3205U/SW270C/SW321C/SW271C/EX480UZ/PD2705UA/PD3205UA/PD2706U/PD2706UA)"
    const checkDescriptionFour="OSD menu>System>USB Awake>ON"
    const checkDescriptionFive="(PD3200U/PD3200Q/PD2700U)"
    const checkDescriptionSix="OSD menu>System>Power Awake> Thunderbolt3 / USB > ON"
    const checkDescriptionSeven="(PD3220U/PD2720U/PD2725U/PD2506Q)"
    const checkDescriptionEight="Note: the monitor is a passive device and won't supply power actively to USB devices except required USB accessories."

    const url = "application/monitor-faq-kn-00036.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0  || description.indexOf(checkDescriptionSeven)<0  || description.indexOf(checkDescriptionEight)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0  || description.indexOf(checkDescriptionSeven)<0  || description.indexOf(checkDescriptionEight)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00036 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00036 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2506Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW271C, SW321C,  PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00036.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00036 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00036 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2506Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2720U, PD2725U, PD3200Q, PD3200U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW271C, SW321C,  PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00036.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00036 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00039
Given("Open the page successfully on application-monitor-faq-kn-00039",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00039.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00039.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00039 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00039 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00039",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="When connecting to a MacBook Pro 2019 and switching USB-C configuration from USB 3.1 Gen1 to USB 2.0, the monitor sometimes doesn't get a signal from randomly. Why is that?"
    const url = "application/monitor-faq-kn-00039.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00039 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00039 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00039",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="Data output from MacBook Pro 2019 becomes unstable when the USB-C port is switched from USB 3.1 to the USB 2.0 standard. Restart your Mac to resolve the issue.  (You could keep the monitor’s USB 2.0 configuration and don’t need to revert to USB 3.1)"

    const url = "application/monitor-faq-kn-00039.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00039 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00039 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2705U, PD2705UA, PD3205U, PD3205UA, PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00039.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00039 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00039 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2705U, PD2705UA, PD3205U, PD3205UA, PD2706U, PD2706UA"
    const url = "application/monitor-faq-kn-00039.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00039 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//troubleshooting/monitor-faq-kn-00006
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00006.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00006.html
    const url = "troubleshooting/monitor-faq-kn-00006.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00006.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00006 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why can’t I select 4K resolution on my Mac device on my BenQ 4K monitor?"
    const url = "troubleshooting/monitor-faq-kn-00006.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="Regarding the lack of a 4K option in Mac OS via USB-C or DisplayPort, the reason may be that the Mac needs 4-lane bandwidth Main Link to reach 4K resolution on external monitors. Otherwise, a no 4K option issue may occur if DisplayPort/USB-C provides only 2-lane bandwidth. Thus, we suggest you follow these steps as a workaround.  "
    const checkDescriptionTwo="For SW271C/SW321C/SW271:"
    const checkDescriptionThree="Go to OSD menu > System > USB-C Configuration > UHD@60Hz + USB 2.0"
    const checkDescriptionFour="For SW271C/SW321C/SW271:"
    const checkDescriptionFive="Go to OSD menu > System > USB-C Configuration > USB 2.0"    
    const url = "troubleshooting/monitor-faq-kn-00006.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 ||description.indexOf(checkDescriptionThree)<0||description.indexOf(checkDescriptionFour)<0||description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 ||description.indexOf(checkDescriptionThree)<0||description.indexOf(checkDescriptionFour)<0||description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2705U, PD2705UA, PD3205U, PD3205UA, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00006.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX480UZ, PD2705U, PD2705UA, PD3205U, PD3205UA, SW271, SW271C, SW321C, PD2706U, PD2706UA"
    const url = "troubleshooting/monitor-faq-kn-00006.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// explanation/monitor-faq-kn-00020
Given("Open the page successfully on explanation-monitor-faq-kn-00020",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00020.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00020.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation/monitor-faq-kn-00020 failURL: ",failURL)
    console.log("Open the page successfully on explanation/monitor-faq-kn-00020 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00020",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is the difference between PD2705U / PD3205U / PD2706U and PD2705UA / PD3205UA / PD2706UA"
    const url = "explanation/monitor-faq-kn-00020.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00020 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation/monitor-faq-kn-00020 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00020",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="The difference is the stand type. "
    const checkDescriptionTwo="PD2705U/PD3205U/PD2706U is Height Adjustable Stand (HAS), as most PD series designs."
    const checkDescriptionThree="PD2705UA/PD3205UA/PD2706UA is Ergo Stand with a new clamp design."

    const url = "explanation/monitor-faq-kn-00020.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree || description.indexOf(checkDescriptionFour)<0 || description.indexOf(checkDescriptionFive)<0 || description.indexOf(checkDescriptionSix)<0)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)

    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00020 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00020 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00020",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705U, PD2705UA, PD3205U, PD3205UA,  PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00020.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00020 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00020 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00020",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705U, PD2705UA, PD3205U, PD3205UA,  PD2706U, PD2706UA"
    const url = "explanation/monitor-faq-kn-00020.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00020 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation/monitor-faq-kn-00020 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00050
Given("Open the page successfully on application-monitor-faq-kn-00050",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00050.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00050.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00050 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00050 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00050",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I set up dual monitors with one Thunderbolt 3 port?"
    const url = "application/monitor-faq-kn-00050.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00050 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00050 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00050",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="To daisy chain two monitors via Thunderbolt, connect the two monitors with a Thunderbolt 3/4 certified cable and follow the steps below."
    const checkDescriptionTwo="Power on all monitors."
    const checkDescriptionThree="Connect the Thunderbolt out on your laptop to the Thunderbolt in on monitor 1."
    const checkDescriptionFour="Connect the Thunderbolt out on monitor 1 to the Thunderbolt in on monitor 2"

    const url = "application/monitor-faq-kn-00050.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00050 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00050 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2720U, PD3220U, PD2725U"
    const url = "application/monitor-faq-kn-00050.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fmonitor-faq-kn-00050 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00050 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2720U, PD3220U, PD2725U"
    const url = "application/monitor-faq-kn-00050.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00050 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00008
Given("Open the page successfully on application-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00008.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00008.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What cables provide the best performance for Mac M1/M2?"
    const url = "application/monitor-faq-kn-00008.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00008.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3270U, EW3280U, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW321C"
    const url = "application/monitor-faq-kn-00008.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3270U, EW3280U, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW321C"
    const url = "application/monitor-faq-kn-00008.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00008 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00051
Given("Open the page successfully on application-monitor-faq-kn-00051",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00051.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00051.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00051 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00051 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00051",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why cannot my Macbook M1 Pro, Macbook M1 Max , and Mac Studio M1 Max and M1 Ultra recognize two individual monitors with the same model name in Display Pilot?"
    const url = "application/monitor-faq-kn-00051.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00051 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00051 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00051",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescription="Please download version 1.1.2.4 of Display Pilot or a later version, it can fix the issue."

    const url = "application/monitor-faq-kn-00051.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00051 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00051 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q"
    const url = "application/monitor-faq-kn-00051.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00051 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00051 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, PD2500Q, PD2506Q, PD2700Q, PD2700U, PD2705Q, PD2705U, PD2705UA, PD2706U, PD2706UA, PD2720U, PD2725U, PD3205U, PD3205UA, PD3220U, PD3420Q"
    const url = "application/monitor-faq-kn-00051.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00051 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00009
Given("Open the page successfully on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00009.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="My BenQ monitor doesn’t support USB-C. Can it connect to an M1/M2 MacBook® via a Thunderbolt 3/4 (USB-C) to DisplayPort or HDMI adapter?"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2381T, BL2420PT, BL2480T, BL2581T, BL2780T, EL2870U, EW2480, EW2780, EW2780Q, EX240, EX240N, EX2510, EX2510S, EX270M, EX270QM, EX2710, EX2710Q, EX2710R, EX2710S, EX2710U, EX3210R, EX3210U, EX3410R, EX3415R, GL2480, GL2580H, GL2780, GW2280, GW2283, GW2480, GW2780, PD2500Q, PD2700Q, PD2700U, PD2705Q, PD3200Q, PD3200U, SW240, SW2700PT"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2381T, BL2420PT, BL2480T, BL2581T, BL2780T, EL2870U, EW2480, EW2780, EW2780Q, EX240, EX240N, EX2510, EX2510S, EX270M, EX270QM, EX2710, EX2710Q, EX2710R, EX2710S, EX2710U, EX3210R, EX3210U, EX3410R, EX3415R, GL2480, GL2580H, GL2780, GW2280, GW2283, GW2480, GW2780, PD2500Q, PD2700Q, PD2700U, PD2705Q, PD3200Q, PD3200U, SW240, SW2700PT"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00007
Given("Open the page successfully on application-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00007.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00007.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00007 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Is DisplayPilot supported on Apple M1/M2?"
    const url = "application/monitor-faq-kn-00007.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00007 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00007.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00007 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot"
    const url = "application/monitor-faq-kn-00007.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot"
    const url = "application/monitor-faq-kn-00007.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00007 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00006
Given("Open the page successfully on application-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00006.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00006.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Is Paper Color Sync (PCS) supported on Apple M1/M2?"
    const url = "application/monitor-faq-kn-00006.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00006.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Paper Color Sync"
    const url = "application/monitor-faq-kn-00006.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Paper Color Sync"
    const url = "application/monitor-faq-kn-00006.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00006 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00029
Given("Open the page successfully on application-monitor-faq-kn-00029",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00029.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00029.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00029 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00029 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00029",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Is Palette Master Element (PME) supported on Apple M1/M2?"
    const url = "application/monitor-faq-kn-00029.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00029 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00029 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00029",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00029.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00029 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00029 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Palette Master Element"
    const url = "application/monitor-faq-kn-00029.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00029 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00029 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Palette Master Element"
    const url = "application/monitor-faq-kn-00029.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00029 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00005
Given("Open the page successfully on application-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00005.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Do I need to use Rosetta or Rosetta 2 on Apple M1/M2 devices with support for BenQ software?"
    const url = "application/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, Palette Master Element, Paper Color Sync"
    const url = "application/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="Display Pilot, Palette Master Element, Paper Color Sync"
    const url = "application/monitor-faq-kn-00005.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00013
Given("Open the page successfully on application-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00013.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00013.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application/monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Open the page successfully on application/monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Do BenQ monitors work with Mac M1/M2?"
    const url = "application/monitor-faq-kn-00013.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00013",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00013.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00013 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00013 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//application/monitor-faq-kn-00052
Given("Open the page successfully on application-monitor-faq-kn-00052",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00052.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00052.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application/monitor-faq-kn-00052 failURL: ",failURL)
    console.log("Open the page successfully on application/monitor-faq-kn-00052 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00052",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I connect a Mac® M1/M2 to BenQ monitors?"
    const url = "application/monitor-faq-kn-00052.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00052 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00052 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00052",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00052.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00052 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00052 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//application/monitor-faq-kn-00053
Given("Open the page successfully on application-monitor-faq-kn-00053",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00053.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00053.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application/monitor-faq-kn-00053 failURL: ",failURL)
    console.log("Open the page successfully on application/monitor-faq-kn-00053 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00053",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Are BenQ monitors compatible with Apple Mac M1/M2?"
    const url = "application/monitor-faq-kn-00053.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00053 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00053 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00053",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00053.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00053 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00053 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//application/monitor-faq-kn-00011
Given("Open the page successfully on application-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00011.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why is my BenQ monitor locked at 4K 30Hz/2K 60Hz or not showing 4K 60Hz/2K 144Hz when connected to a Mac M1/M2?"
    const url = "application/monitor-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please access the BenQ Knowledge Center "
    const checkDescriptionTwo="https://www.benq.com/en-us/knowledge-center/knowledge/how-do-i-connect-my-mac-m1-to-benq-monitor.html"
    const checkDescriptionThree="to get more information."

    const url = "application/monitor-faq-kn-00011.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX2780Q, EW2780U, EW3280U, PD2720U, PD3220U"
    const url = "application/monitor-faq-kn-00011.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX2780Q, EW2780U, EW3280U, PD2720U, PD3220U"
    const url = "application/monitor-faq-kn-00011.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00011 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00076
Given("Open the page successfully on application-projector-faq-kn-00076",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00076
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00076
    const url = "application/projector-faq-kn-00076.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00076.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application/projector-faq-kn-00076 failURL: ",failURL)
    console.log("Open the page successfully on application/projector-faq-kn-00076 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00076",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I set up ARC on a BenQ projector?"
    const url = "application/projector-faq-kn-00076.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application/projector-faq-kn-00076 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application/projector-faq-kn-00076 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00076",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const checkDescriptionOne="Please visit the link below and follow the instructions provided to set up ARC functionality."
    const checkDescriptionTwo="Or, please refer to the following troubleshooting steps if there's no audio from your speaker through ARC on the projector."
    const checkDescriptionThree="Check if the HDMI cable you use supports ARC/eARC"
    const checkDescriptionFour="If the cable does not support ARC/eARC, please change an HDMI cable that support ARC/eARC. "
    const checkDescriptionFive="If the cable supports ARC/eARC, please go to step 2."
    const checkDescriptionSix="Check if the HDMI port on the projector and your device both support ARC/eARC"
    const checkDescriptionSeven="If they don’t, please change to HDMI port which supports ARC/eARC"
    const checkDescriptionEight="If ARC/eARC is supported, please go to step 3."
    const checkDescriptionNine="Check if both SPDIF and Audio Return are enabled on the projector"
    const checkDescriptionTen="If SPDIF and Audio Return are not enabled, please enable SPDIF and Audio Return on the projector. "
    const checkDescriptionEleven="If they are already enabled, please go to step 4."
    const checkDescriptionTwelve="Check if Audio Return is enabled on your video source device"
    const checkDescriptionThirteen="If Audio Return is not enabled, please enable it on your video source device."
    const checkDescriptionFourteen="If it is already enabled, please go to step 5."
    const checkDescriptionFifteen="Check if the audio format you would like is supported by your device. "
    const checkDescriptionSixteen="If the audio format is not supported, please contact the manufacturer of your device."
    const checkDescriptionSeventeen="If audio format is supported, please contact BenQ with the following information for further investigation if there is still no audio from your speaker even with the above process."
    const checkDescriptionEighteen="The setting of ARC/eARC on the BenQ projector, input source device, and  audio output device"
    const checkDescriptionNineteen="How devices and projectors are connected"
    const checkDescriptionTwenty="The model number of the video source device and audio output device"
    const checkDescriptionTwentyOne="Cable length and materialThe audio format from the input source"
    const checkDescriptionTwentyTwo="Media platform of input source "
    const checkDescriptionTwentyThree="Firmware version running on the projector"
    // <inset video: https://www.youtube.com/watch?v=yft6wAG_VbA>
    const checkVideo="yft6wAG_VbA"


    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    const url = "application/projector-faq-kn-00126"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 ||description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0 ||description.indexOf(checkDescriptionEleven)<0 ||description.indexOf(checkDescriptionTwelve)<0 ||description.indexOf(checkDescriptionThirteen)<0 ||description.indexOf(checkDescriptionFourteen)<0 ||description.indexOf(checkDescriptionFifteen)<0 ||description.indexOf(checkDescriptionSixteen)<0 ||description.indexOf(checkDescriptionSeventeen)<0 ||description.indexOf(checkDescriptionEighteen)<0 ||description.indexOf(checkDescriptionNineteen)<0 ||description.indexOf(checkDescriptionTwenty)<0 ||description.indexOf(checkDescriptionTwentyOne)<0 ||description.indexOf(checkDescriptionTwentyTwo)<0 ||description.indexOf(checkDescriptionTwentyThree)<0 ||video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 ||description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0 ||description.indexOf(checkDescriptionEleven)<0 ||description.indexOf(checkDescriptionTwelve)<0 ||description.indexOf(checkDescriptionThirteen)<0 ||description.indexOf(checkDescriptionFourteen)<0 ||description.indexOf(checkDescriptionFifteen)<0 ||description.indexOf(checkDescriptionSixteen)<0 ||description.indexOf(checkDescriptionSeventeen)<0 ||description.indexOf(checkDescriptionEighteen)<0 ||description.indexOf(checkDescriptionNineteen)<0 ||description.indexOf(checkDescriptionTwenty)<0 ||description.indexOf(checkDescriptionTwentyOne)<0 ||description.indexOf(checkDescriptionTwentyTwo)<0 ||description.indexOf(checkDescriptionTwentyThree)<0 ||video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00076 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00076 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL} `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GK100, GP500, GS50, HT2650i, HT3550, HT3550i, LK936ST, LK954ST, TH685i, TK810, TK850, TK850i, V6000, V6050, V7000i, V7050i, W1800, W2700i"
    const url = "application/projector-faq-kn-00076.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00076 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00076 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GK100, GP500, GS50, HT2650i, HT3550, HT3550i, LK936ST, LK954ST, TH685i, TK810, TK850, TK850i, V6000, V6050, V7000i, V7050i, W1800, W2700i"
    const url = "application/projector-faq-kn-00076.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00076 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00070
Given("Open the page successfully on application-projector-faq-kn-00070",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00070
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00070
    const url = "application/projector-faq-kn-00070.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00070.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application/projector-faq-kn-00070 failURL: ",failURL)
    console.log("Open the page successfully on application/projector-faq-kn-00070 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00070",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How do I use the projector to watch Netflix?"
    const url = "application/projector-faq-kn-00070.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application/projector-faq-kn-00070 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application/projector-faq-kn-00070 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00070",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const checkDescriptionOne="Yes, it is possible to do so, please follow the instruction below."
    const checkDescriptionTwo="Before starting, please make sure you’ve updated the firmware to the latest version."
    const checkDescriptionThree="Download “Apps Manager” from Google play on your projector"
    const checkDescriptionFour="Download “Netflix” app"
    const checkDescriptionFive="Allow all Netflix access"
    const checkDescriptionSix="Important! Never update the Netflix version and always click the “CANCEL” button to ensure the use of Netflix."
    const checkDescriptionSeven="Please use “BenQ Smart Control app” on your phone for navigation"
    const checkDescriptionEight="Or, follow the instruction video below to install Netflix after the firmware is updated to the latest version:"
    const checkDescriptionNine="For the most user-friendly experience, make sure to download"
    const checkDescriptionTen="BenQ Smart Control"
    const checkDescriptionEleven="app via Google Play or App Store to control Netflix smoothly. Learn more about BenQ Smart Control app:"
    const checkDescriptionTwelve="https://www.benq.com/en-us/knowledge-center/knowledge/how-to-start-benq-smart-home-projector.html#benq-smart-control-app"
    const checkDescriptionThirteen="*NOTE: You CAN NOT cast Netflix on the projector via any wireless casting because of copyright protection policies."

    // < insert viddeo: https://www.youtube.com/watch?v=SOHWDAGS70o >
    const checkVideo="SOHWDAGS70o"


    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00126.html
    const url = "application/projector-faq-kn-00126"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 ||description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0 ||description.indexOf(checkDescriptionEleven)<0 ||description.indexOf(checkDescriptionTwelve)<0 ||description.indexOf(checkDescriptionThirteen)<0||video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 ||description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0 ||description.indexOf(checkDescriptionEleven)<0 ||description.indexOf(checkDescriptionTwelve)<0 ||description.indexOf(checkDescriptionThirteen)<0||video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00070 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00070 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL} `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GP500, GS50, GV11, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i"
    const url = "application/projector-faq-kn-00070.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GP500, GS50, GV11, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i"
    const url = "application/projector-faq-kn-00070.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00070 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});


//application/monitor-faq-kn-00047
Given("Open the page successfully on application-monitor-faq-kn-00047",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/monitor-faq-kn-00047.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00047.html
    const url = "application/monitor-faq-kn-00047.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00047.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00047 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00047 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00047",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why are HDMI Auto Switch and DP Auto Switch greyed out in the OSD menu?"
    const url = "application/monitor-faq-kn-00047.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00047 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00047 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00047",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="HDMI and DP Auto Switch become available in the OSD menu if you select"
    const checkDescriptionTwo="None"
    const checkDescriptionThree="under Video Input in the KVM Switch section of the menu."
    const url = "application/monitor-faq-kn-00047.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne) || description.indexOf(checkDescriptionTwo) ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne) || description.indexOf(checkDescriptionTwo) ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00047 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00047 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705Q"
    const url = "application/monitor-faq-kn-00047.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD2705Q"
    const url = "application/monitor-faq-kn-00047.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00047 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});


//application/monitor-faq-kn-00048
Given("Open the page successfully on application-monitor-faq-kn-00048",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/monitor-faq-kn-00048.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00048.html
    const url = "application/monitor-faq-kn-00048.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00048.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00048 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00048 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00048",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Can I use the Eye Care and Bright Intelligence function on every GW2480 input port and Picture Mode, or is there any restriction?"
    const url = "application/monitor-faq-kn-00048.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00048 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00048 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00048",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="All input ports of GW2480 support the Eye Care and Bright Intelligence function available on the OSD menu."
    const checkDescriptionTwo="However, the Bright Intelligence function does not work if you set the Picture Mode to the following modes:"
    const checkDescriptionThree="sRGB/ECO/User. The Bright Intelligence function will be turned “OFF” automatically even if you have set the function to “ON”."
    const url = "application/monitor-faq-kn-00048.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne) || description.indexOf(checkDescriptionTwo) ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne) || description.indexOf(checkDescriptionTwo) ||description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00048 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00048 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GW2480"
    const url = "application/monitor-faq-kn-00048.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GW2480"
    const url = "application/monitor-faq-kn-00048.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00048 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});


//application/monitor-faq-kn-00049
Given("Open the page successfully on application-monitor-faq-kn-00049",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/monitor-faq-kn-00049.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00049.html
    const url = "application/monitor-faq-kn-00049.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00049.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00049 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00049 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00049",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How to adjust the brightness in Eco mode?"
    const url = "application/monitor-faq-kn-00049.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00049 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00049 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00049",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="In GL2760H, the brightness in Eco mode can not be adjusted using the OSD menu. If you wish to set the favorite brightness level, please switch to Standard mode first, then use the Contract or Brightness function from the OSD menu to adjust brightness."
    const url = "application/monitor-faq-kn-00049.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00049 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00049 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GL2760H"
    const url = "application/monitor-faq-kn-00049.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GL2760H"
    const url = "application/monitor-faq-kn-00049.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00049 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

