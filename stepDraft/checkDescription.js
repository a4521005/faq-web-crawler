//有video
Then("Compare description to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const checkDescription="The input lag of GS50 is 22.7ms (1080P@60Hz) under Game Mode, please visit the link below and follow the instruction to carry out firmware upgrades for your projectors. "
    // const checkVideo="https://youtu.be/ON4oAwaGhCM"
    const checkVideo="ON4oAwaGhCM"


    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 || video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 || video.indexOf(checkVideo)<0 ){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL} `)
    }
});

//只有文字
Then("Compare description to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="This results from the process of encoding and decoding audio and is unavoidable. Due to physics and distance, absolute zero latency is impossible, but there are two main ways to minimize lip sync issues:"
    const checkDescriptionTwo="Use an aptX or aptX-LL Bluetooth device to play audio. Note BenQ projectors only support the SBC (Sub-band Codec) audio coding format."
    const checkDescriptionThree="Instead of Bluetooth, consider using wired speakers or headphones."

    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

//有PHOTO-1
Then("Compare description to check pass or fail on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescription="The fiber optic audio plug is one-directional, meaning it can only go into the connector one way. Please check the connector carefully before connecting the SPDIF audio cable to the SPDIF connector on the projector. Do not force the cable in, as this may damage the cable plug and/or the optical port on the projector."
    const checkPhotoOne="SPDIF1"
    const checkPhotoTwo="SPDIF2"
    const url = "application/projector-faq-kn-00094.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photoOne = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photoTwo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                descriptionandphotofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }

            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandphotofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});


//有PHOTO-2
Then("Compare description to check pass or fail on explanation-projector-faq-kn-00028",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescriptionOne="The displayed image ratio shrinks because the projector adjusts the displayed image ratio based on a received resolution which does not match the output resolutions the projector is designed for."
    const checkDescriptionTwo="Please follow the steps below if you would like to have the displayed image remain intact:"
    const checkDescriptionThree="Step 1"    
    const checkDescriptionFour="Go to Windows Settings and select Display" 
    const checkDescriptionFive="Step 2"    
    const checkDescriptionSix="Select Advanced Display Settings" 
    const checkDescriptionSeven="Step 3"    
    const checkDescriptionEight="Select Display 2: BenQ PJ, then click Display Adapter Properties for Display 2"     
    const checkDescriptionNine="Step 4"    
    const checkDescriptionTen="Select List All Modes, then choose an output resolution that your projector is designed for (as listed)"         
    const checkPhotoOne="Duplicate Mode_1."
    const checkPhotoTwo="Duplicate Mode_2"
    const checkPhotoThree="Duplicate Mode_3"
    const checkPhotoFour="Duplicate Mode_4"
    const checkPhotoFive="Duplicate Mode_5"
    const url = "explanation/projector-faq-kn-00028.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0){
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }

            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            const photo= await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer', element => element.innerHTML);
            
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0 || description.indexOf(checkDescriptionFour)<0 ||description.indexOf(checkDescriptionFive)<0 ||description.indexOf(checkDescriptionSix)<0 ||description.indexOf(checkDescriptionSeven)<0 ||description.indexOf(checkDescriptionEight)<0 ||description.indexOf(checkDescriptionNine)<0 ||description.indexOf(checkDescriptionTen)<0){
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    failURL.push(testUrl)
                    descriptionfailURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    descriptionandphotofailURL.push(testUrl)
                }
                
            }else{
                if(description.indexOf(checkPhotoOne)>0 && photo.indexOf(checkPhotoTwo)>0 && photo.indexOf(checkPhotoThree)>0 && photo.indexOf(checkPhotoFour)>0 && photo.indexOf(checkPhotoFive)>0){
                    passURL.push(testUrl)
                }else{
                    failURL.push(testUrl)
                    photofailURL.push(testUrl)
                }

            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandphotofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }    
});