//have show more
Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2381T, BL2411PT, BL2420PT, BL2423PT, BL2480, BL2480L, BL2480T, BL2480TL, BL2485TC, BL2581T, BL2706HT, BL2711U, BL2780, BL2780T, BL2785TC, EW2480, EW2770QZ, EW2780, EW2780Q, EW2780U, EW2880U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2710U, EX2780Q, EX3210U, EX3415R, GW2283, GW2381, GW2406Z, GW2475H, GW2480, GW2480EL, GW2480L, GW2480T, GW2480TL, GW2485TC, GW2765HE, GW2765HT, GW2780, GW2780T, GW2785TC, PD2500Q, PD2700Q, PD2700QT, PD2700U, PD2705Q, PD2705U, PD2710QC, PD2720U, PD2725U, PD3200U, PD3200UE, PD3205U, PD3220U, PD3420Q, PG2401PT, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C, EX240, PD2506Q"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//no show more
Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2485TC, BL2785TC, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2780Q, EX3203R, EX3501R, GW2485TC, GW2785TC, PD2506Q, PD2705Q, PD2705U, PD2705UA, PD2710QC, PD3205U, PD3205UA, PD3420Q, SW270C, SW271, SW271C, SW321C, GW2790QT, BL2790QT, GW3290QT, BL3290QT"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3280U, EX2510, EX2710, EX2780Q, EX3415R, EX2710Q, EX2710R, EX3210R, EW3880R, EX2510S, EX2710S, EW2880U, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "troubleshooting/monitor-faq-kn-00014.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});