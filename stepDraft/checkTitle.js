Then("[EN-site]Compare title to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (title.indexOf("How do I upgrade firmware for better input lag experience of GS50?")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (title.indexOf("How do I upgrade firmware for better input lag experience of GS50?")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    
    console.log("[EN-site]Compare title to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[EN-site]Compare title to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});