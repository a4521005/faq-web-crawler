Given("[Global]open the page successfully on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    //en-us
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    const enusTestUrl= benqEuWeb+"en-us"+supportUrl+url
    await this.page.goto(enusTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enusInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enusInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enusTestUrl)
    }else{
        passURL.push(enusTestUrl)
    }
    //en-hk
    const enhkTestUrl= benqEuWeb+"en-hk"+supportUrl+url
    await this.page.goto(enhkTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enhkInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enhkInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enhkTestUrl)
    }else{
        passURL.push(enhkTestUrl)
    }
    //en-ca
    const encaTestUrl= benqEuWeb+"en-ca"+supportUrl+url
    await this.page.goto(encaTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const encaInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (encaInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(encaTestUrl)
    }else{
        passURL.push(encaTestUrl)
    }
    //en-ap
    const enapTestUrl= benqEuWeb+"en-ap"+supportUrl+url
    await this.page.goto(enapTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enapInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enapInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enapTestUrl)
    }else{
        passURL.push(enapTestUrl)
    }
    //en-au
    const enauTestUrl= benqEuWeb+"en-au"+supportUrl+url
    await this.page.goto(enauTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enauInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enauInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enauTestUrl)
    }else{
        passURL.push(enauTestUrl)
    }
    //en-my
    const enmyTestUrl= benqEuWeb+"en-my"+supportUrl+url
    await this.page.goto(enmyTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enmyInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enmyInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enmyTestUrl)
    }else{
        passURL.push(enmyTestUrl)
    }
    //en-in
    const eninTestUrl= benqEuWeb+"en-in"+supportUrl+url
    await this.page.goto(eninTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const eninInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (eninInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(eninTestUrl)
    }else{
        passURL.push(eninTestUrl)
    }
    //en-sg
    const ensgTestUrl= benqEuWeb+"en-sg"+supportUrl+url
    await this.page.goto(ensgTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const ensgInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (ensgInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(ensgTestUrl)
    }else{
        passURL.push(ensgTestUrl)
    }
    //en-me
    const enmeTestUrl= benqEuWeb+"en-me"+supportUrl+url
    await this.page.goto(enmeTestUrl+cicGA)
    await this.page.waitForSelector('html')
    const enmeInnerHtml = await this.page.$eval('html', element => element.innerHTML);
    if (enmeInnerHtml.indexOf("faq-par-wrap")<0){
        failURL.push(enmeTestUrl)
    }else{
        passURL.push(enmeTestUrl)
    }
    // //benq.eu
    // for(var i=0; i<b2cBenqEuEnRegions.length; i++){
    //     const b2cRegionsCode = b2cBenqEuEnRegions[i]
    //     const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
    //     await this.page.goto(testUrl+cicGA)
    //     await this.page.waitForSelector('html')
    //     const innerHtml = await this.page.$eval('html', element => element.innerHTML);
    //     // await this.page.waitForSelector("body > div.faq-par-wrap")
    //     if (innerHtml.indexOf("faq-par-wrap")<0){
    //         failURL.push(testUrl)
    //     }else{
    //         passURL.push(testUrl)
    //     }
    // }

    console.log("specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});