Feature:B2C BenQ FAQ Web Crawler
    1.the URL can open the pages successfully
    2.
    if EN site:
    Compare title to check pass or fail
    Compare description to check pass or fail
    Compare Model to check pass or fail
    else non-EN site:
    Compare Model to check pass or fail

# specification/projector-faq-kn-00075
    Scenario: specification/projector-faq-kn-00075
        Given Open the page successfully on specification-projector-faq-kn-00075
    Scenario: [Titles][EN-site]specification/projector-faq-kn-00075
        Then Compare title to check pass or fail on specification-projector-faq-kn-00075
    Scenario: [Description][EN-site]specification/projector-faq-kn-00075
        Then Compare description to check pass or fail on specification-projector-faq-kn-00075
    Scenario: [Models][EN-site]specification/projector-faq-kn-00075 on en site
        Then [EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075
    Scenario: [Models][non-EN site]specification/projector-faq-kn-00075 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075

# application/projector-faq-kn-00001
    Scenario: application/projector-faq-kn-00001
        Given Open the page successfully on application-projector-faq-kn-00001
    Scenario: [Titles][EN-site]application/projector-faq-kn-00001
        Then Compare title to check pass or fail on application-projector-faq-kn-00001
    Scenario: [Description][EN-site]application/projector-faq-kn-00001
        Then Compare description to check pass or fail on application-projector-faq-kn-00001
    Scenario: [Models][EN-site]application/projector-faq-kn-00001 on en site
        Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001
    Scenario: [Models][non-EN site]application/projector-faq-kn-00001 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001

# application/projector-faq-kn-00092
    Scenario: application/projector-faq-kn-00092
        Given Open the page successfully on application-projector-faq-kn-00092
    Scenario: [Titles][EN-site]application/projector-faq-kn-00092
        Then Compare title to check pass or fail on application-projector-faq-kn-00092
    Scenario: [Description][EN-site]application/projector-faq-kn-00092
        Then Compare description to check pass or fail on application-projector-faq-kn-00092
    Scenario: [Models][EN-site]application/projector-faq-kn-00092 on en site
        Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092
    Scenario: [Models][non-EN site]application/projector-faq-kn-00092 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092


# application/projector-faq-kn-00093
    Scenario: application/projector-faq-kn-00093
        Given Open the page successfully on application-projector-faq-kn-00093
    Scenario: [Titles][EN-site]application/projector-faq-kn-00093
        Then Compare title to check pass or fail on application-projector-faq-kn-00093
    Scenario: [Description][EN-site]application/projector-faq-kn-00093
        Then Compare description to check pass or fail on application-projector-faq-kn-00093
    Scenario: [Models][EN-site]application/projector-faq-kn-00093 on en site
        Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093
    Scenario: [Models][non-EN site]application/projector-faq-kn-00093 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093

# explanation/projector-faq-kn-00017
    Scenario: explanation/projector-faq-kn-00017
        Given Open the page successfully on explanation-projector-faq-kn-00017
    Scenario: [Titles][EN-site]explanation/projector-faq-kn-00017
        Then Compare title to check pass or fail on explanation-projector-faq-kn-00017
    Scenario: [Description][EN-site]explanation/projector-faq-kn-00017
        Then Compare description to check pass or fail on explanation-projector-faq-kn-00017
    Scenario: [Models][EN-site]explanation/projector-faq-kn-00017 on en site
        Then [EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017
    Scenario: [Models][non-EN site]explanation/projector-faq-kn-00017 on non-en site
        Then [non-EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017

# # application/projector-faq-kn-00094
#     Scenario: application/projector-faq-kn-00094
#         Given Open the page successfully on application-projector-faq-kn-00094
#     Scenario: [Titles][EN-site]application/projector-faq-kn-00094
#         Then Compare title to check pass or fail on application-projector-faq-kn-00094
#     Scenario: [Description][EN-site]application/projector-faq-kn-00094
#         Then Compare description to check pass or fail on application-projector-faq-kn-00094
#     Scenario: [Models][EN-site]application/projector-faq-kn-00094 on en site
#         Then [EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094
#     Scenario: [Models][non-EN site]application/projector-faq-kn-00094 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094

# # troubleshooting/projector-faq-kn-00035
#     Scenario: application/troubleshooting/projector-faq-kn-00035
#         Given Open the page successfully on troubleshooting-projector-faq-kn-00035
#     Scenario: [Titles][EN-site]application/troubleshooting/projector-faq-kn-00035
#         Then Compare title to check pass or fail on troubleshooting-projector-faq-kn-00035
#     Scenario: [Description][EN-site]application/troubleshooting/projector-faq-kn-00035
#         Then Compare description to check pass or fail on troubleshooting-projector-faq-kn-00035
#     Scenario: [Models][EN-site]application/troubleshooting/projector-faq-kn-00035 on en site
#         Then [EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035
#     Scenario: [Models][non-EN site]application/troubleshooting/projector-faq-kn-00035 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035

# # application/monitor-faq-kn-00033
#     Scenario: application/monitor-faq-kn-00033
#         Given Open the page successfully on application-monitor-faq-kn-00033
#     Scenario: [Titles][EN-site]application/monitor-faq-kn-00033
#         Then Compare title to check pass or fail on application-monitor-faq-kn-00033
#     Scenario: [Description][EN-site]application/monitor-faq-kn-00033
#         Then Compare description to check pass or fail on application-monitor-faq-kn-00033
#     Scenario: [Models][EN-site]application/monitor-faq-kn-00033 on en site
#         Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033
#     Scenario: [Models][non-EN site]application/monitor-faq-kn-00033 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033

# # application/monitor-faq-kn-00032
#     Scenario: application/monitor-faq-kn-00032
#         Given Open the page successfully on application-monitor-faq-kn-00032
#     Scenario: [Titles][EN-site]application/monitor-faq-kn-00032
#         Then Compare title to check pass or fail on application-monitor-faq-kn-00032
#     Scenario: [Description][EN-site]application/monitor-faq-kn-00032
#         Then Compare description to check pass or fail on application-monitor-faq-kn-00032
#     Scenario: [Models][EN-site]application/monitor-faq-kn-00032 on en site
#         Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032
#     Scenario: [Models][non-EN site]application/monitor-faq-kn-00032 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032

# # application/monitor-faq-kn-00023
#     Scenario: application/monitor-faq-kn-00023
#         Given Open the page successfully on application-monitor-faq-kn-00023
#     Scenario: [Titles][EN-site]application/monitor-faq-kn-00023
#         Then Compare title to check pass or fail on application-monitor-faq-kn-00023
#     Scenario: [Description][EN-site]application/monitor-faq-kn-00023
#         Then Compare description to check pass or fail on application-monitor-faq-kn-00023
#     Scenario: [Models][EN-site]application/monitor-faq-kn-00023 on en site
#         Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023
#     Scenario: [Models][non-EN site]application/monitor-faq-kn-00023 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023

# # application/monitor-faq-kn-00009
#     Scenario: application/monitor-faq-kn-00009
#         Given Open the page successfully on application-monitor-faq-kn-00009
#     Scenario: [Titles][EN-site]application/monitor-faq-kn-00009
#         Then Compare title to check pass or fail on application-monitor-faq-kn-00009
#     Scenario: [Description][EN-site]application/monitor-faq-kn-00009
#         Then Compare description to check pass or fail on application-monitor-faq-kn-00009
#     Scenario: [Models][EN-site]application/monitor-faq-kn-00009 on en site
#         Then [EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009
#     Scenario: [Models][non-EN site]application/monitor-faq-kn-00009 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009

# # explanation/monitor-faq-kn-00017
#     Scenario: explanation/monitor-faq-kn-00017
#         Given Open the page successfully on explanation-monitor-faq-kn-00017
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00017
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00017
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00017
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00017
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00017 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00017 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017

# # explanation/monitor-faq-kn-00016
#     Scenario: explanation/monitor-faq-kn-00016
#         Given Open the page successfully on explanation-monitor-faq-kn-00016
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00016
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00016
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00016
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00016
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00016 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00016 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016


# # explanation/monitor-faq-kn-00015
#     Scenario: explanation/monitor-faq-kn-00015
#         Given Open the page successfully on explanation-monitor-faq-kn-00015
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00015
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00015
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00015
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00015
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00015 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00015 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015


# # explanation/monitor-faq-kn-00014
#     Scenario: explanation/monitor-faq-kn-00014
#         Given Open the page successfully on explanation-monitor-faq-kn-00014
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00014
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00014
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00014
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00014
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00014 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00014 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014

# # explanation/monitor-faq-kn-00002
#     Scenario: explanation/monitor-faq-kn-00002
#         Given Open the page successfully on explanation-monitor-faq-kn-00002
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00002
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00002
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00002
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00002
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00002 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00002 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002

# # explanation/monitor-faq-kn-00005
#     Scenario: explanation/monitor-faq-kn-00005
#         Given Open the page successfully on explanation-monitor-faq-kn-00005
#     Scenario: [Titles][EN-site]explanation/monitor-faq-kn-00005
#         Then Compare title to check pass or fail on explanation-monitor-faq-kn-00005
#     Scenario: [Description][EN-site]explanation/monitor-faq-kn-00005
#         Then Compare description to check pass or fail on explanation-monitor-faq-kn-00005
#     Scenario: [Models][EN-site]explanation/monitor-faq-kn-00005 on en site
#         Then [EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005
#     Scenario: [Models][non-EN site]explanation/monitor-faq-kn-00005 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005

# # specification/monitor-faq-kn-00010
#     Scenario: specification/monitor-faq-kn-00010
#         Given Open the page successfully on specification-monitor-faq-kn-00010
#     Scenario: [Titles][EN-site]specification/monitor-faq-kn-00010
#         Then Compare title to check pass or fail on specification-monitor-faq-kn-00010
#     Scenario: [Description][EN-site]specification/monitor-faq-kn-00010
#         Then Compare description to check pass or fail on specification-monitor-faq-kn-00010
#     Scenario: [Models][EN-site]specification/monitor-faq-kn-00010 on en site
#         Then [EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010
#     Scenario: [Models][non-EN site]specification/monitor-faq-kn-00010 on non-en site
#         Then [non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010

# # troubleshooting/monitor-faq-kn-00004
#     Scenario: troubleshooting/monitor-faq-kn-00004
#         Given Open the page successfully on troubleshooting-monitor-faq-kn-00004
#     Scenario: [Titles][EN-site]troubleshooting/monitor-faq-kn-00004
#         Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00004
#     Scenario: [Description][EN-site]troubleshooting/monitor-faq-kn-00004
#         Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00004
#     Scenario: [Models][EN-site]troubleshooting/monitor-faq-kn-00004
#         Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004
#     Scenario: [Models][non-EN site]troubleshooting/monitor-faq-kn-00004
#         Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004

# # troubleshooting/monitor-faq-kn-00003
#     Scenario: troubleshooting/monitor-faq-kn-00003
#         Given Open the page successfully on troubleshooting-monitor-faq-kn-00003
#     Scenario: [Titles][EN-site]troubleshooting/monitor-faq-kn-00003
#         Then Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00003
#     Scenario: [Description][EN-site]troubleshooting/monitor-faq-kn-00003
#         Then Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00003
#     Scenario: [Models][EN-site]troubleshooting/monitor-faq-kn-00003
#         Then [EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003
#     Scenario: [Models][non-EN site]troubleshooting/monitor-faq-kn-00003
#         Then [non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003