const { Given, When, Then, Before, After } = require('@cucumber/cucumber')
const { expect, use } = require('chai')
const fs = require('fs');
const puppeteer = require('puppeteer')
const request = require("request-promise");
// const request = require("request-promise-native");
const cheerio = require("cheerio");

const b2cBenqComEnRegions = [
    "en-us",
    "en-hk",
    "en-ca",
    "en-ap",
    "en-au",
    "en-my",
    "en-in",
    "en-sg",
    "en-me"
]
const b2cBenqEuEnRegions = [
    "en-eu",
    "en-uk",
    "en-cee",
    "en-no",
    "en-lu",
    "en-ie",
    "en-hr",
    "en-rs",
    "en-ee",
    "en-lv",
    "en-si",
    "en-mt",
    "en-ba",
    "en-mk",
    "en-cy",
    "en-fi",
    "en-dk",
    "en-ie"
    
]
const b2cBenqComNonEnRegions = [
    "zh-hk",
    "ru-ru",
    "fr-ca",
    "es-mx",
    "pt-br",
    "es-ar",
    "es-co",
    "es-pe",
    "es-la",
    "es-cl",
    "ar-me",
    "tr-tr",
    "id-id",
    "th-th",
    "ja-jp",
    "ko-kr",
    "vi-vn",
    "zh-tw",
    
]
const b2cBenqEuNonEnRegions = [
    "es-es",
    "it-it",
    "fr-fr",
    "nl-nl",
    "de-de",
    "pl-pl",
    "cs-cz",
    "pt-pt",
    "sv-se",
    "el-gr",
    "ro-ro",
    "sk-sk",
    "uk-ua",
    "bg-bg",
    "hu-hu",
    "lt-lt",
    "nl-be",
    "de-at",
    "de-ch"
    
]
const b2cBenqZhcnRegions = [
    "zh-cn"
]

const benqComWeb = "https://www.benq.com/"
const benqEuWeb = "https://www.benq.eu/"
const benqZhcnWeb = "https://www.benq.com.cn/"


const supportUrl = "/support/downloads-faq/faq/product/"
const cicGA="?utm_source=autotest&utm_medium=CIC"

Before({timeout: 24 * 5000},async function () {
    this.browser = await puppeteer.launch({ 
        executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
        // MAC路徑:
        // executablePath:
        // '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
        headless:true,//有無需要開視窗,false要開,true不開
        slowMo:100,// slow down by 100ms
        devtools:false//有無需要開啟開發人員工具
    })
    this.page = await this.browser.newPage()
    await this.page.setViewport({width:1440,height:1000})
    await this.page.setDefaultTimeout(100000000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    await this.page.setDefaultNavigationTimeout(100000000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
})

After({timeout: 12 * 5000},async function () {
    await this.browser.close()
})

//specification/projector-faq-kn-00075
Given("Open the page successfully on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/projector-faq-kn-00075.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Open the page successfully on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How do I upgrade firmware for better input lag experience of GS50?"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="The input lag of GS50 is 22.7ms (1080P@60Hz) under Game Mode, please visit the link below and follow the instruction to carry out firmware upgrades for your projectors."
    // const checkVideo="https://youtu.be/ON4oAwaGhCM"
    const checkVideo="ON4oAwaGhCM"
    const url = "specification/projector-faq-kn-00075.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.video', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("videofailURL: ",videofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50"
    const url = "specification/projector-faq-kn-00075.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("faqrelated")>0){
                const modelCheck = 'body > div.faqrelated'
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification-projector-faq-kn-00075 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00001
Given("Open the page successfully on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00001.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00001.html
    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00001.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="When I connect my projector with a Bluetooth speaker, there is a lip sync problem. How can I fix it?"
    const url = "application/projector-faq-kn-00001.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="This results from the process of encoding and decoding audio and is unavoidable. Due to physics and distance, absolute zero latency is impossible, but there are two main ways to minimize lip sync issues:"
    const checkDescriptionTwo="Use an aptX or aptX-LL Bluetooth device to play audio. Note BenQ projectors only support the SBC (Sub-band Codec) audio coding format."
    const checkDescriptionThree="Instead of Bluetooth, consider using wired speakers or headphones."

    const url = "application/projector-faq-kn-00001.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div > div > p > span', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div > div > p > span', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    // const checkModel=[
    //     "EH600", 
    //     "EW600", 
    //     "EW800ST", 
    //     "EX600", 
    //     "EX800ST", 
    //     "GS1", 
    //     "GS2", 
    //     "GS50", 
    //     "GV1", 
    //     "GV30", 
    //     "HT2650i", 
    //     "HT3550i", 
    //     "QS01", 
    //     "TH685i", 
    //     "TK700STi", 
    //     "TK810", 
    //     "TK850i", 
    //     "V7000i", 
    //     "V7050i", 
    //     "W1800i", 
    //     "W2700i", 
    //     "X1300i", 
    //     "X3000i", 
    //     "GV11"
    // ]
    const checkModel="EH600, EW600, EW800ST, EX600, EX800ST, GS1, GS2, GS50, GV1, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK810, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00001.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EH600, EW600, EW800ST, EX600, EX800ST, GS1, GS2, GS50, GV1, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK810, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application-projector-faq-kn-00001.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00001 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00092
Given("Open the page successfully on application-projector-faq-kn-00092",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00092.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00092.html
    const url = "application/projector-faq-kn-00092.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00092.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00092 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00092 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00092",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I reboot the Android TV dongle?"
    const url = "application/projector-faq-kn-00092.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00092 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00092 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00092",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="Please visit this link to learn how to reboot the Android TV dongle:"
    // const checkVideo="https://youtu.be/6CzRZXQzltM"
    const checkVideo="6CzRZXQzltM"
    const url = "application/projector-faq-kn-00092.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.column-control.parbase > div > div > div:nth-child(2) > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.column-control.parbase > div > div > div:nth-child(2) > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("videofailURL: ",videofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00092 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00092 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00092.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00092.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00092 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00093
Given("Open the page successfully on application-projector-faq-kn-00093",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00093.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00093.html
    const url = "application/projector-faq-kn-00093.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00093.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00093 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00093 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00093",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I change the device name for the Android TV dongle?"
    const url = "application/projector-faq-kn-00093.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00093 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00093 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00093",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const videofailURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="Please visit this link to learn how to reboot the Android TV dongle:"
    // const checkVideo="https://youtu.be/eL8HJXTa9SI"
    const checkVideo="eL8HJXTa9SI"
    const url = "application/projector-faq-kn-00093.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.column-control.parbase > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const video = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.column-control.parbase > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                videofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && video.indexOf(checkVideo)<0){
                failURL.push(testUrl)
                descriptionandvideofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("videofailURL: ",videofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00093 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00093 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00093.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00093.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00093 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// explanation/projector-faq-kn-00017
Given("Open the page successfully on explanation-projector-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/projector-faq-kn-00017.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00017.html
    const url = "explanation/projector-faq-kn-00017.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/projector-faq-kn-00017.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-projector-faq-kn-00017 failURL: ",failURL)
    console.log("Open the page successfully on explanation-projector-faq-kn-00017 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-projector-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Does the Android TV dongle support Fast Roaming?"
    const url = "explanation/projector-faq-kn-00017.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-projector-faq-kn-00017 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-projector-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-projector-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="Yes. Fast roaming is supported by the BenQ Android TV dongle"
    const url = "explanation/projector-faq-kn-00017.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on explanation-projector-faq-kn-00017 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-projector-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "explanation/projector-faq-kn-00017.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "explanation/projector-faq-kn-00017.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-projector-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/projector-faq-kn-00094
Given("Open the page successfully on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/projector-faq-kn-00094.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00094.html
    const url = "application/projector-faq-kn-00094.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/projector-faq-kn-00094.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("Open the page successfully on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How do I plug the SPDIF audio cable into the SPDIF connector?"
    const url = "application/projector-faq-kn-00094.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescription="The fiber optic audio plug is one-directional, meaning it can only go into the connector one way. Please check the connector carefully before connecting the SPDIF audio cable to the SPDIF connector on the projector. Do not force the cable in, as this may damage the cable plug and/or the optical port on the projector."
    const checkPhotoOne="SPDIF1"
    const checkPhotoTwo="SPDIF2"
    const url = "application/projector-faq-kn-00094.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const photoOne = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div:nth-child(2)', element => element.innerHTML);
            const photoTwo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div:nth-child(3)', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                descriptionandphotofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase > div > p:nth-child(1) > span', element => element.innerHTML);
            const photoOne = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div:nth-child(2)', element => element.innerHTML);
            const photoTwo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div:nth-child(3)', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)>0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)>0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && photoOne.indexOf(checkPhotoOne)<0 && photoTwo.indexOf(checkPhotoTwo)<0){
                failURL.push(testUrl)
                descriptionandphotofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="all"
    const url = "application/projector-faq-kn-00094.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "application/projector-faq-kn-00094.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-projector-faq-kn-00094 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// troubleshooting/projector-faq-kn-00035
Given("Open the page successfully on troubleshooting-projector-faq-kn-00035",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "troubleshooting/projector-faq-kn-00035.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/projector-faq-kn-00035.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-projector-faq-kn-00035 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-projector-faq-kn-00035 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-projector-faq-kn-00035",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="I cannot use the Android TV dongle's remote control to control Android TV system or my projector? How can I fix it?"
    const url = "troubleshooting/projector-faq-kn-00035.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-projector-faq-kn-00035 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-projector-faq-kn-00035 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-projector-faq-kn-00035",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Please follow the steps below to do troubleshooting:"
    const checkDescriptionTwo="Power cycle the projector and wait for 5 mins then check if the issue is still up."
    const checkDescriptionThree="Check if the Android TV dongle (QS01) is installed in the specific HDMI port. If not, please fit in the dongle and check again."
    const checkDescriptionFour="Check if “BenQ HDMI Media Streaming (QS01)” is “ON” in the OSD menu. If not, please turn it ON and power cycle the projector."
    const checkDescriptionFive="QS01%OSD-HDMI%Streaming"
    const checkDescriptionSix="Check if you can call out the “OSD menu” with “Menu” key of the projector keypad. If you can’t, please contact BenQ for further assistance."
    const checkDescriptionSeven="Check if you can see “All Projector Settings” on home screen. If you can’t, please contact BenQ for further assistance."
    const checkDescriptionEight="QS01%Settings"
    const checkDescriptionNine="Press and hold “OK” to pair the remote control."
    const checkDescriptionTen="If the remote control flashes RED lighting: Please long press the “OK” button on the remote control to pair it with the Android TV dongle when the pairing page pops up."
    const checkDescriptionEleven="If the remote control flashes BLUE lighting: Please check whether the status is “Connected” or not (check the status under “Android TV setting > Remote & Accessories”). If not, please unpair the remote control, and pair the remote control with the Android TV dongle again. "
    const checkDescriptionTwelve="QS01%pairing"
    const checkDescriptionThirteen="Check if the “Prime Video” button on the remote control is working or not. If not, please contact BenQ for further assistance."
    const checkDescriptionFourteen="Check the “Source” button on the remote control work or not. If not, please contact BenQ for further assistance."
    const checkDescriptionFifteen="QS01%flow"

    const url = "troubleshooting/projector-faq-kn-00035.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0 || description.indexOf(checkDescriptionNine)<0 || description.indexOf(checkDescriptionTen)<0 || description.indexOf(checkDescriptionEleven)<0 || description.indexOf(checkDescriptionTwelve)<0 || description.indexOf(checkDescriptionThirteen)<0 || description.indexOf(checkDescriptionFourteen)<0 || description.indexOf(checkDescriptionFifteen)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0 || description.indexOf(checkDescriptionNine)<0 || description.indexOf(checkDescriptionTen)<0 || description.indexOf(checkDescriptionEleven)<0 || description.indexOf(checkDescriptionTwelve)<0 || description.indexOf(checkDescriptionThirteen)<0 || description.indexOf(checkDescriptionFourteen)<0 || description.indexOf(checkDescriptionFifteen)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-projector-faq-kn-00035 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-projector-faq-kn-00035 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "troubleshooting/projector-faq-kn-00035.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "troubleshooting/projector-faq-kn-00035.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-projector-faq-kn-00035 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00033
Given("Open the page successfully on application-monitor-faq-kn-00033",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/monitor-faq-kn-00033.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00033.html
    const url = "application/monitor-faq-kn-00033.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00033.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00033 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00033 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00033",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="Why doesn't my monitor wake up when I press the controller keys of the BenQ HotKey Puck G1/G2?"
    const url = "application/monitor-faq-kn-00033.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00033 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00033 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00033",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="The monitor must receive video input from the desktop/laptop to wake up from sleep mode."
    const checkDescriptionTwo="Pressing any keys on HotKey Puck won't wake the monitor up."

    const url = "application/monitor-faq-kn-00033.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00033 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00033 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    // const checkModel=[
    //     "EH600", 
    //     "EW600", 
    //     "EW800ST", 
    //     "EX600", 
    //     "EX800ST", 
    //     "GS1", 
    //     "GS2", 
    //     "GS50", 
    //     "GV1", 
    //     "GV30", 
    //     "HT2650i", 
    //     "HT3550i", 
    //     "QS01", 
    //     "TH685i", 
    //     "TK700STi", 
    //     "TK810", 
    //     "TK850i", 
    //     "V7000i", 
    //     "V7050i", 
    //     "W1800i", 
    //     "W2700i", 
    //     "X1300i", 
    //     "X3000i", 
    //     "GV11"
    // ]
    const checkModel="PD3200Q, PD3200U, SW2700PT, SW271, SW320, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271C, SW321C"
    const url = "application/monitor-faq-kn-00033.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="PD3200Q, PD3200U, SW2700PT, SW271, SW320, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271C, SW321C"
    const url = "application-monitor-faq-kn-00033.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00033 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00032
Given("Open the page successfully on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00032.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00032.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is IPS glow and how can I make it less visible?"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Due to the technology used on IPS panels, some monitors have inconsistent brightness when observed from different angles and distances. This appears like a glow that’s somewhat visible when the screen is either all black or showing dark content. As IPS glow is technology-related, it cannot be completely prevented or eliminated. Thus, it has been widely accepted as a unique feature of IPS panels. This phenomenon is exclusive to IPS monitors and is far more common than TN/VA light bleed, although not as severe when it does happen."
    const checkDescriptionTwo="There are some setup tips that you can try to make the IPS glow less visible:"
    const checkDescriptionThree="Increase the ambient lighting in the room where the monitor is being used."
    const checkDescriptionFour="Lower the brightness level of the monitor via the OSD menu."
    const checkDescriptionFive="Adjust the height and tilt of the monitor until you find the right angle where the glow is least visible."
    const checkDescriptionSix="Place the monitor a little bit further from your seating position."

    const url = "application/monitor-faq-kn-00032.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2381T, BL2411PT, BL2420PT, BL2423PT, BL2480, BL2480L, BL2480T, BL2480TL, BL2485TC, BL2581T, BL2706HT, BL2711U, BL2780, BL2780T, BL2785TC, EW2480, EW2770QZ, EW2780, EW2780Q, EW2780U, EW2880U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2710U, EX2780Q, EX3210U, EX3415R, GW2283, GW2381, GW2406Z, GW2475H, GW2480, GW2480EL, GW2480L, GW2480T, GW2480TL, GW2485TC, GW2765HE, GW2765HT, GW2780, GW2780T, GW2785TC, PD2500Q, PD2700Q, PD2700QT, PD2700U, PD2705Q, PD2705U, PD2710QC, PD2720U, PD2725U, PD3200U, PD3200UE, PD3205U, PD3220U, PD3420Q, PG2401PT, PV270, PV3200PT, SW240, SW2700PT, SW270C, SW271, SW271C, SW320, SW321C, EX240, PD2506Q"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00032.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00032 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00023
Given("Open the page successfully on application-monitor-faq-kn-00023",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/application/monitor-faq-kn-00023.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00023.html
    const url = "application/monitor-faq-kn-00023.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00023.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00023 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00023 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00023",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How can I disable FreeSync on a BenQ monitor?"
    const url = "application/monitor-faq-kn-00023.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00023 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00023 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00023",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="FreeSync is always active on the monitor by default. Please toggle FreeSync on/off from the graphics card control center."
    const checkDescriptionTwo="Once off on the driver side, the monitor will also disable FreeSync."

    const url = "application/monitor-faq-kn-00023.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00023 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00023 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    // const checkModel=[
    //     "EH600", 
    //     "EW600", 
    //     "EW800ST", 
    //     "EX600", 
    //     "EX800ST", 
    //     "GS1", 
    //     "GS2", 
    //     "GS50", 
    //     "GV1", 
    //     "GV30", 
    //     "HT2650i", 
    //     "HT3550i", 
    //     "QS01", 
    //     "TH685i", 
    //     "TK700STi", 
    //     "TK810", 
    //     "TK850i", 
    //     "V7000i", 
    //     "V7050i", 
    //     "W1800i", 
    //     "W2700i", 
    //     "X1300i", 
    //     "X3000i", 
    //     "GV11"
    // ]
    const checkModel="EX2510, EX2710, EL2870U, EW2480, EW2780, EW3270U, EW3280U, EX2780Q, EX3200R, EX3501R, EX3415R, EX2710Q, EX2710R, EX3210R, EX2510S, EX2710S, EX3410R, EX3210U,EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00023.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX2510, EX2710, EL2870U, EW2480, EW2780, EW3270U, EW3280U, EX2780Q, EX3200R, EX3501R, EX3415R, EX2710Q, EX2710R, EX3210R, EX2510S, EX2710S, EX3410R, EX3210U,EX2710U, EX240, EX240N"
    const url = "application-monitor-faq-kn-00023.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00023 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//application/monitor-faq-kn-00009
Given("Open the page successfully on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "application/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00009.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Open the page successfully on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="My BenQ monitor doesn’t support USB-C. Can it connect to an M1 MacBook via a Thunderbolt 3 (USB-C) to DisplayPort or HDMI adapter?"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare title to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="If your BenQ monitor uses HDMI, DisplayPort, and Mini DisplayPort for video output => input, it will work with any properly-certified and made USB-C to DisplayPort or HDMI adapters. But in order to get the best experience, we suggest you buy "
    const checkDescriptionTwo="Thunderbolt 3 (or USB-C Alt Mode) compatible monitors."
    const checkDescriptionThree="monitor.html?connectivity=thunderbolt-3,usb-type-c"

    const url = "application/monitor-faq-kn-00009.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("Compare description to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX3415R, EX2510, EX2710, EW2780, SW240, SW2700PT, EW2480, EW2780Q, BL2381T, BL2581T, BL2283, BL2480T, BL2780T, GW2283, GW2480, GW2780, GL2780, GL2480, GL2580H, GW2280, PD2705Q, PD2700U, EL2870U, PD3200U, PD2700Q, PD3200Q, PD2500Q, BL2420PT,  EX2710Q, EX2710R, EX3210R, EX2510S, EX2710S, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX3415R, EX2510, EX2710, EW2780, SW240, SW2700PT, EW2480, EW2780Q, BL2381T, BL2581T, BL2283, BL2480T, BL2780T, GW2283, GW2480, GW2780, GL2780, GL2480, GL2580H, GW2280, PD2705Q, PD2700U, EL2870U, PD3200U, PD2700Q, PD3200Q, PD2500Q, BL2420PT,  EX2710Q, EX2710R, EX3210R, EX2510S, EX2710S, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "application/monitor-faq-kn-00009.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on application-monitor-faq-kn-00009 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});


//explanation/monitor-faq-kn-00017
Given("Open the page successfully on explanation-monitor-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00017.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00017.html
    const url = "explanation/monitor-faq-kn-00017.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00017.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00017 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00017 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is the difference between DisplayHDR and HDR10?"
    const url = "explanation/monitor-faq-kn-00017.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00017 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="DisplayHDR version 1.0 focuses on liquid crystal displays (LCDs), establishing three distinct levels of HDR system performance to facilitate adoption of HDR throughout the PC market: DisplayHDR 400, DisplayHDR 600, and DisplayHDR 1000. HDR10 refers to a standard adopted widely by most monitor brands, and supports the compressed transmission of HDR video content. Monitors of all levels are required to support the industry standard HDR10 format in order to properly display HDR content."
    const url = "explanation/monitor-faq-kn-00017.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00017 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW2780, EW2780Q, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710R, EX2710S, EX2780Q, EX3203R, EX3210R, EX3210U, EX3410R, EX3415R, EX3501R, PD2700U, PD2705Q, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW320, SW321C, EX2710U, EX240, EX240N, PD2506Q"
    const url = "explanation/monitor-faq-kn-00017.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW2780, EW2780Q, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710R, EX2710S, EX2780Q, EX3203R, EX3210R, EX3210U, EX3410R, EX3415R, EX3501R, PD2700U, PD2705Q, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW320, SW321C, EX2710U, EX240, EX240N, PD2506Q"
    const url = "explanation/monitor-faq-kn-00017.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00017 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// explanation/monitor-faq-kn-00016
Given("Open the page successfully on explanation-monitor-faq-kn-00016",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00016.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00016.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00016 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00016 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00016",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is HDR10?"
    const url = "explanation/monitor-faq-kn-00016.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00016 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00016 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00016",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="HDR10 is a standard adopted widely by most global monitor brands, as many major companies have implemented the HDR10 protocol. Defined jointly by the Blu-Ray Association, HDMI Forum, and UHD Association, HDR10 supports the compressed transmission of HDR video content. HDR10 was officially defined as a format that supports HDR content by the Consumer Electronics Association (CEA) on August 27, 2015. One of the key factors to fulfill HDR requirements on a monitor is the ability to decode files in the HDR10 format."
    const checkDescriptionTwo="HDR10 files need to meet the following criteria:"
    const checkDescriptionThree="1. EOTF (electro-optical transfer function) using SMPTE ST2084"
    const checkDescriptionFour="2. Color sub-sampling: 4:2:2/4:2:0 (for compressed video sources)"
    const checkDescriptionFive="3. Bit depth: 10-bit"
    const checkDescriptionSix="4. Primary color: ITU-R BT.2020"
    const checkDescriptionSeven="5. Metadata: SMPTE ST2086, MaxFALL, MaxCLL"
    const checkDescriptionEight='**SMPTE ST2086 "Mastering Display Color Volume" static metadata sends color calibration data from the mastering display, such as MaxFALL (Maximum Frame Average Light Level) and MaxCLL (Maximum Content Light Level) static values, encoded as SEI messages within the video stream.'

    const url = "explanation/monitor-faq-kn-00016.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00016 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00016 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW2780, EW2780Q, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710R, EX2710S, EX2780Q, EX3203R, EX3210R, EX3210U, EX3410R, EX3415R, EX3501R, PD2700U, PD2705Q, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW320, SW321C, EX2710U, EX240, EX240N, PD2506Q"
    const url = "explanation/monitor-faq-kn-00016.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW2780, EW2780Q, EW2780U, EW2880U, EW3270U, EW3280U, EW3880R, EX2510, EX2510S, EX2710, EX2710Q, EX2710R, EX2710S, EX2780Q, EX3203R, EX3210R, EX3210U, EX3410R, EX3415R, EX3501R, PD2700U, PD2705Q, PD2720U, PD2725U, PD3220U, PD3420Q, SW270C, SW271, SW271C, SW320, SW321C, EX2710U, EX240, EX240N, PD2506Q"
    const url = "explanation/monitor-faq-kn-00016.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00016 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00015
Given("Open the page successfully on explanation-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "explanation/monitor-faq-kn-00015.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/application/monitor-faq-kn-00015.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What's the difference between CinemaHDRi and GameHDRi?"
    const url = "explanation/monitor-faq-kn-00015.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Both Cinema HDRi and Game HDRi are types of the HDRi Technology."
    const checkDescriptionTwo="Cinema HDRi"
    const checkDescriptionThree="enhances contrast and the color performance so as to avoid washed-out images. By optimizing color saturation, enrichment, and image clarity, Cinema HDRi gets you there in the cinematic world."
    const checkDescriptionFour="Game HDRi"
    const checkDescriptionFive="enhances the contrast to the images and the color gradations to avoid overexposure. Game HDRi optimizes image fidelity and clarity for gaming visuals best for most game scenarios."

    const url = "explanation/monitor-faq-kn-00015.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3280U, EX2510, EX2710, EX2780Q, EX3415R, EX2710Q, EX2710R, EX3210R, EW3880R, EX2510S, EX2710S, EW2880U, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00015.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00015.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00015 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00014
Given("Open the page successfully on explanation-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00014.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00014.html
    const url = "explanation/monitor-faq-kn-00014.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00014.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00014 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What benefits does BenQ HDRi Technology offer?"
    const url = "explanation/monitor-faq-kn-00014.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="Exclusive BenQ HDRi technology delivers improved contrast, detail, and refined colors compared to standard HDR to ensure the best gaming experience and video enjoyment."
    const url = "explanation/monitor-faq-kn-00014.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3280U, EX2510, EX2710, EX2780Q, EX3415R, EX2710Q, EX2710R, EX3210R, EW3880R, EX2510S, EX2710S, EW2880U, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00014.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EW2780U, EW3280U, EX2510, EX2710, EX2780Q, EX3415R, EX2710Q, EX2710R, EX3210R, EW3880R, EX2510S, EX2710S, EW2880U, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00014.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00014 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00002
Given("Open the page successfully on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00002.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00002.html
    const url = "explanation/monitor-faq-kn-00002.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00002.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00002 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What is emulated HDR?"
    const url = "explanation/monitor-faq-kn-00002.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00002 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescriptionOne="Emulated HDR generates a virtual HDR effect for SDR content. If content uses standard dynamic range but you still want to experience high dynamic range (HDR) visual performance, emulated HDR helps improve your viewing enjoyment. All you need to do is turn on HDR mode via the OSD. "
    const checkDescriptionTwo="Once the monitor detects non-HDR content, it  automatically turns on emulated HDR. "
    const url = "explanation/monitor-faq-kn-00002.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescriptionOne)>0 && description.indexOf(checkDescriptionTwo)>0){
                passURL.push(testUrl)
            }else{
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)>0 && description.indexOf(checkDescriptionTwo)>0){
                passURL.push(testUrl)
            }else{
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00002 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW277HDR, EW2780U, EW3270U, EW3280U, EX2510, EX2710, EX2780Q, EX3203R, EX3501R, PD2700U, PD2720U, PD3220U, SW271, SW320, EW2880U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00002.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW277HDR, EW2780U, EW3270U, EW3280U, EX2510, EX2710, EX2780Q, EX3203R, EX3501R, PD2700U, PD2720U, PD3220U, SW271, SW320, EW2880U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00002.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00002 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//explanation/monitor-faq-kn-00005
Given("Open the page successfully on explanation-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00005.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00005.html
    const url = "explanation/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/explanation/monitor-faq-kn-00005.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Open the page successfully on explanation-monitor-faq-kn-00005 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on explanation-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="How does the inclusion of a digital signal processor or DSP chip benefit users?"
    const url = "explanation/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare title to check pass or fail on explanation-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on explanation-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]
    const descriptionandvideofailURL=[]

    const checkDescription="The DSP chip makes it possible to optimize various sound effects by enhancing or moderating audio. It also provides better noise cancelation and optimal tuning for accurate aural performance based on different user listening needs."
    const url = "explanation/monitor-faq-kn-00005.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescription)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("descriptionandvideofailURL: ",descriptionandvideofailURL)
    
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("Compare description to check pass or fail on explanation-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX2510, EX2710,  EX2510S, EX2710S, EX2710Q, EX2710R, EX3210R, EX3410R, EX3210U,EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00005.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }
            else{
                failURL.push(testUrl)
            }

        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EX2510, EX2710, EX2510S, EX2710S, EX2710Q, EX2710R, EX3210R, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "explanation/monitor-faq-kn-00005.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const modelCheck = 'body > div.faqrelated'
            const modelOnWeb="body > div.faqrelated > div > p"
            const model = await this.page.$eval(modelCheck, element => element.innerHTML);
            const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
            // console.log(".com model:",model)
            // console.log(".com modelActual:",modelActual)
            if (model.indexOf(checkModel)<0){
                failURL.push(testUrl)
                console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

            }else{
                passURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on explanation-monitor-faq-kn-00005 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//specification/monitor-faq-kn-00010
Given("Open the page successfully on specification-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "specification/monitor-faq-kn-00010.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/specification/monitor-faq-kn-00010.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on specification-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Open the page successfully on specification-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on specification-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="What are the differences between Eye Protect, Bright Intelligence, and Bright Intelligence Plus?"
    const url = "specification/monitor-faq-kn-00010.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on specification-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Compare title to check pass or fail on specification-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on specification-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="Eye Protect:"
    const checkDescriptionTwo="built-in light sensor detects light conditions around the monitor and adjusts the backlight automatically."
    const checkDescriptionThree="Bright Intelligence (B.I.):"
    const checkDescriptionFour="built-in light sensor works to detect the ambient light, image brightness, and contrast, and then balances screen settings automatically."
    const checkDescriptionFive="Bright Intelligence Plus (B.I.+):"
    const checkDescriptionSix="same as above but more advanced sensor also takes color temperature into account when optimizing screen settings for the best image quality."



    const url = "specification/monitor-faq-kn-00010.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("Compare description to check pass or fail on specification-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "specification/monitor-faq-kn-00010.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="BL2283, BL2423PT, BL2480, BL2480T, BL2483, BL2483T, BL2581T, BL2780, BL2780T, BL2783, EL2870U, EW2480, EW2775ZH, EW277HDR, EW2780, EW2780Q, EW2780U, EW3270U, EW3270ZL, EW3280U, EX2510, EX2510S, EX2710, EX2710Q, EX2710S, EX2780Q, EX3203R, EX3210U, EX3410R, EX3415R, GL2460BH, GL2480, GL2780, GW2280, GW2283, GW2381, GW2480, GW2480T, GW2780, GW2780T, PD2500Q, PD2700U, PD2705Q, PD3200Q, PD3200U, PV270, PV3200PT,EX2710U, EX240, EX240N"
    const url = "specification/monitor-faq-kn-00010.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on specification-monitor-faq-kn-00010 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

// troubleshooting/monitor-faq-kn-00004
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00004",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const url = "troubleshooting/monitor-faq-kn-00004.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00004.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00004 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00004 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00004",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle='Why does the message "Emulated HDR" appear on the monitor when playing HDR content?'
    const url = "troubleshooting/monitor-faq-kn-00004.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00004 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00004 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00004",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const descriptionfailURL=[]

    const checkDescriptionOne="This message means that the input signal is not HDR. Please make sure that the input content, device and cable are HDR compatible. "
    const checkDescriptionTwo="Please follow the below steps to verify:"
    const checkDescriptionThree="Make sure the video content is encoded with HDR by looking for the HDR description or logo. For example, for YouTube, the video quality setting should indicate “HDR.” For streaming services such as Netflix, the HDR logo will be displayed next to HDR compatible content."
    const checkDescriptionFour="Check the internet speed and ensure it is fast enough to stream HDR content. For HDR content in 4K resolution, the download speed should be higher than 25 megabits per second."
    const checkDescriptionFive="Check the HDR compatibility of source device. The firmware of source device should be updated to the latest version and support HDR and HDCP 2.2."
    const checkDescriptionSix="Verify whether the cables support HDR. The cables that come with BenQ HDR monitors are recommended for the best video quality. If an aftermarket cable is used, make sure it fits one the following requirements:"
    const checkDescriptionSeven="HDMI: HDMI 1.4 or later versions"
    const checkDescriptionEight="DisplayPort/DP to MiniDP: DP 1.2 or later versions"
    const checkDescriptionNine="USB Type C: USB-C certified"
    const checkDescriptionTen="For more information, please refer to: httcps://www.benq.com/en/knowledge-center/knowledge/hdr-troubleshooting-instruction.html"

    const url = "troubleshooting/monitor-faq-kn-00004.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0 || description.indexOf(checkDescriptionNine)<0 || description.indexOf(checkDescriptionTen)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div', element => element.innerHTML);
            if (description.indexOf(checkDescriptionOne)<0 || description.indexOf(checkDescriptionTwo)<0 || description.indexOf(checkDescriptionThree)<0  || description.indexOf(checkDescriptionFour)<0  || description.indexOf(checkDescriptionFive)<0  || description.indexOf(checkDescriptionSix)<0 || description.indexOf(checkDescriptionSeven)<0 || description.indexOf(checkDescriptionEight)<0 || description.indexOf(checkDescriptionNine)<0 || description.indexOf(checkDescriptionTen)<0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00004 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00004 passURL: ",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW277HDR, EW2780U, EW3270U, EW3280U, EX2510, EX2710, EX2780Q, EX3203R, EX3501R, PD2700U, PD2720U, PD3220U, SW271, SW320, EX3415R, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "troubleshooting/monitor-faq-kn-00004.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForTimeout(10000)
            // const closeCookie = "#btn_close"
            // if(innerHtml.indexOf("btn_close")>0){
            //     await this.page.click(closeCookie)
            //     await this.page.waitForTimeout(5000)
            // }
            // await this.page.waitForTimeout(10000)
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="EL2870U, EW2480, EW277HDR, EW2780U, EW3270U, EW3280U, EX2510, EX2710, EX2780Q, EX3203R, EX3501R, PD2700U, PD2720U, PD3220U, SW271, SW320, EX3415R, EX3410R, EX3210U, EX2710U, EX240, EX240N"
    const url = "troubleshooting/monitor-faq-kn-00004.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }else{
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)
    
                    }else{
                        passURL.push(testUrl)
                    }
                }else{
                    failURL.push(testUrl)
                }
            }
        }
    }
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00004 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});

//troubleshooting/monitor-faq-kn-00003
Given("Open the page successfully on troubleshooting-monitor-faq-kn-00003",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    //https://www.benq.com/en-us/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00003.html
    //https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00003.html
    const url = "troubleshooting/monitor-faq-kn-00003.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)

    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }
    //benq.cn
    for(var i=0; i<b2cBenqZhcnRegions.length; i++){
        //await this.page.goto("https://www.benq.com.cn/zh-cn/support/downloads-faq/faq/product/troubleshooting/monitor-faq-kn-00003.html"+cicGA)
        const b2cRegionsCode = b2cBenqZhcnRegions[i]
        const testUrl= benqZhcnWeb+b2cRegionsCode+supportUrl+url
        console.log(testUrl)
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        // await this.page.waitForSelector("body > div.faq-par-wrap")
        if (innerHtml.indexOf("faq-par-wrap")<0){
            failURL.push(testUrl)
        }else{
            passURL.push(testUrl)
        }
    }    
    
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00003 failURL: ",failURL)
    console.log("Open the page successfully on troubleshooting-monitor-faq-kn-00003 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} cannot open on this URL: ${failURL} `)
    }
});

Then("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00003",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkTitle="When connecting the monitor with a Mac, why is LBL (low blue light) color mode disabled and greyed out in the OSD menu? How can I switch on?"
    const url = "troubleshooting/monitor-faq-kn-00003.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const title = await this.page.$eval('body > div.faq-par-wrap > div.faqheader > section > div > h1', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (title.indexOf(checkTitle)<0){
                failURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00003 failURL: ",failURL)
    console.log("Compare title to check pass or fail on troubleshooting-monitor-faq-kn-00003 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} title is fail: ${failURL} `)
    }
});

Then("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00003",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const photofailURL=[]
    const descriptionfailURL=[]
    const descriptionandphotofailURL=[]

    const checkDescription="Please make sure HDR is turned off on your Mac. Starting from MacOS 10.15.4, HDR is automatically activated on your Mac, so the monitor detects HDR and stays in HDR mode, which prevents LBL activation."
    const checkPhoto="monitor-202008-1-mac-hdr"
    const url = "troubleshooting/monitor-faq-kn-00003.html"
    // await this.page.goto(benqComWeb+"en-us"+supportUrl+url+cicGA)
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            const photo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.aemimage', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && photo.indexOf(checkPhoto)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photo.indexOf(checkPhoto)<0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && photo.indexOf(checkPhoto)<0){
                failURL.push(testUrl)
                descriptionandphotofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }

    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            const description = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.text.parbase', element => element.innerHTML);
            const photo = await this.page.$eval('body > div.faq-par-wrap > div.contentcontainer > div > div > div.aemimage', element => element.innerHTML);
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if (description.indexOf(checkDescription)<0 && photo.indexOf(checkPhoto)>0){
                failURL.push(testUrl)
                descriptionfailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)>0 && photo.indexOf(checkPhoto)<0){
                failURL.push(testUrl)
                photofailURL.push(testUrl)
            }else if(description.indexOf(checkDescription)<0 && photo.indexOf(checkPhoto)<0){
                failURL.push(testUrl)
                descriptionandphotofailURL.push(testUrl)
            }else{
                passURL.push(testUrl)
            }
        }
    }
    console.log("descriptionfailURL: ",descriptionfailURL)
    console.log("photofailURL: ",photofailURL)
    console.log("descriptionandvideofailURL: ",descriptionandphotofailURL)
    
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00003 failURL: ",failURL)
    console.log("Compare description to check pass or fail on troubleshooting-monitor-faq-kn-00003 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` ${url} description is fail: ${failURL};  `)
    }
});

Then("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="all"
    const url = "troubleshooting/monitor-faq-kn-00003.html"
    //benq.com
    //EN site
    for(var i=0; i<b2cBenqComEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    //benq.eu
    //en-site
    for(var i=0; i<b2cBenqEuEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            // await this.page.waitForSelector("body > div.faq-par-wrap")
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003 failURL: ",failURL)
    console.log("[EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [EN site] ${url} model is fail: ${failURL} `)
    }
});

Then("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003",{timeout: 120000 * 5000},async function(){
    const passURL=[]
    const failURL=[]
    const checkModel="GS50, GV30, HT2650i, HT3550i, QS01, TH685i, TK700STi, TK850i, V7000i, V7050i, W1800i, W2700i, X1300i, X3000i, GV11"
    const url = "troubleshooting/monitor-faq-kn-00003.html"
    //benq.com
    //non-EN site
    for(var i=0; i<b2cBenqComNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqComNonEnRegions[i]
        const testUrl= benqComWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }
    
    //benq.eu
    //non-EN site
    for(var i=0; i<b2cBenqEuNonEnRegions.length; i++){
        const b2cRegionsCode = b2cBenqEuNonEnRegions[i]
        const testUrl= benqEuWeb+b2cRegionsCode+supportUrl+url
        await this.page.goto(testUrl+cicGA)
        await this.page.waitForSelector('html')
        const innerHtml = await this.page.$eval('html', element => element.innerHTML);
        if (innerHtml.indexOf("faq-par-wrap")>0){
            if(innerHtml.indexOf("show-more")>0){
                const showMoreButton = "body > div.faqrelated > p.faqrelated-show.faqrelated-show-more"
                await this.page.click(showMoreButton)
                await this.page.waitForTimeout(5000)
                if(innerHtml.indexOf("Applicable Models")>0){
                    const modelCheck = 'body > div.faqrelated'
                    const modelOnWeb="body > div.faqrelated > div > p"
                    const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                    const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                    // console.log(".com model:",model)
                    // console.log(".com modelActual:",modelActual)
                    if (model.indexOf(checkModel)<0){
                        failURL.push(testUrl)
                        console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                    }else{
                        passURL.push(testUrl)
                    }
                }
            }
            if(innerHtml.indexOf("Applicable Models")>0){
                const modelCheck = 'body > div.faqrelated'
                const modelOnWeb="body > div.faqrelated > div > p"
                const model = await this.page.$eval(modelCheck, element => element.innerHTML);
                const modelActual = await this.page.$eval(modelOnWeb, element => element.innerHTML);
                // console.log(".com model:",model)
                // console.log(".com modelActual:",modelActual)
                if (model.indexOf(checkModel)<0){
                    failURL.push(testUrl)
                    console.log(`[Fail]${b2cRegionsCode} model on Web : ${modelActual}`)

                }else{
                    passURL.push(testUrl)
                }
            }else{
                failURL.push(testUrl)
            }
        }
    }

    
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003 failURL: ",failURL)
    console.log("[non-EN site]Compare Model to check pass or fail on troubleshooting-monitor-faq-kn-00003 passURL:",passURL)
    if(failURL.length>0){
        throw new Error(` [non-EN site] ${url} model is fail: ${failURL} `)
    }
});